<script type="text/javascript" class="init">
$(document).ready(function() {
    var table = $('#example').DataTable( {
        dom: 'Bfrtip',
        select: true,
        buttons: [
            {
                text: 'Seleccionar',
                action: function () {
                    table.rows().select();
                }
            },
            {
                text: 'Deseleccionar',
                action: function () {
                    table.rows().deselect();
                }
            }
        ]
    } );
} );
</script>

<?php
	require_once("util/utilerias.php");
    $obj = new Utilerias;
	$obj->CnnBD();

    $CAMPUS = $_POST['campus'];
    $TIPCERT = $_POST['tipCert'];    
    $ANHO = $_POST['anho'];
    $FINICIO = $_POST['finicial'];
    $FFINAL = $_POST['ffinal'];
    
    $VALPLANTEL = "INNER JOIN REG_Validacion RVP ON RVP.VP_ID_UNICO_CERT = T1.PEOPLE_CODE_ID+T1.folioControl+T1.Plantel ";    
    
    $query = str_replace("VARIABLE=CAMPUS", $CAMPUS, CERTIFICADOSHEADER);
    $query = str_replace("VARIABLE=TIPO_CERTIFICADO", "AND (RC.TIPO_CERTIFICADO IN (".$TIPCERT."))", $query);
    $query = str_replace("VARIABLE=ANHO", $ANHO, $query);
    $query = str_replace("VARIABLE=FINICIAL", $FINICIO, $query);
    $query = str_replace("VARIABLE=FFINAL", $FFINAL, $query);
	$query = str_replace("VARIABLE=NUMCONTROL", "", $query);
    $query = str_replace("VARIABLE=VALPLANTEL", $VALPLANTEL, $query);
    
	//echo "Query <br>".$query;	
?>
	<link rel='stylesheet' id='compiled.css-css'  href='./css/compiled-4.5.15.min.css' type='text/css' media='all' />
    <script type='text/javascript' src='./js/compiled.0.min.js?ver=4.5.15'></script>
	<script type='text/javascript' src='./js/tablax2.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script>
    // Datatable vertical dynamic height
    $(document).ready(function () {
		$('#dtDynamicVerticalScrollExample').DataTable({
			"scrollY": "45vh",
			"scrollCollapse": true,
			"paging": false
		});

		$('.dataTables_length').addClass('bs-select');

		$(".btn1").click(function(){            
			var vNumControl = $(this).parents("tr").find("td")[2].innerHTML;       						
			var campus = $("#cmbCampus option:selected").text();			
			var finicio = $('#finicio').val();
			var ffinal = $('#ffin').val();
            var anho =  parseInt(finicial.substr(0,4));
			var vNumControl = vNumControl;
            var curiculum = $('#txt'+vNumControl).val();
            xtipCert = $("#cmbTipCertificado option:selected").text();       
            if (xtipCert == "Ambos"){
                tipCert = "'T','P'";
            }else{
                if (xtipCert == "Total"){
                    tipCert = "'T'";    
                }else{
                    tipCert = "'P'";
                }
            }
            $("#ViewInfoAlumno").text("");
			$("#ViewInfoAlumno").load('infoAlumnoC.php?campus=' + campus
            +'&anho=' + anho
			+'&finicio=' + finicio
			+'&ffinal=' + ffinal
			+'&vNumControl=' + vNumControl
            +'&curiculum='+curiculum
            +'&tipCert=' + tipCert            
            );
		});
        
        $(".btn2").click(function() {
			var valores = $(this).parents("tr").find("td")[3].innerHTML;
			console.log(valores);
			//alert(valores);
		});
        
		$(".form-check-input").click(function() {
            if($("#txtGenXML").val().indexOf($(this).parents("tr").find("td")[2].innerHTML) != -1){
                vlCadXML = $("#txtGenXML").val();
                vlCadXML = vlCadXML.replace(",'"+$(this).parents("tr").find("td")[2].innerHTML + "'" , " ");                                
            }else{
                vlCadXML = $("#txtGenXML").val() + ",'" + $(this).parents("tr").find("td")[2].innerHTML + "'";    
            }
            $("#txtGenXML").val(vlCadXML);
		});
        
		$(".btnCancela").click(function(){
            var numunico = $('#txtGenXML').val();
            numunico = numunico.substr(1, 10000);
            alert(numunico);
            formdata = new FormData();    
            formdata.append("numunico", numunico);
            jQuery.ajax({
                url: 'cancela.php',
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success: function (result) {
                    $("#ViewInfoAlumno").text("");
                    $("#ViewInfoAlumno" ).html( result );
                }
            });               
		});	
    }); 
    </script>
    

    <?php
		$rQuery = $obj->xQuery($query);
	?>
    
    <div class="row">
      <div class="col"></div>
      <div class="col"></div>
      <div class="col"><input type="text" id="txtCur" name="txtCur" style="visibility: hidden;"></div>
      <div class="col"><input type="text" id="txtGenXML" name="txtGenXML" ></div>
      <div class="col" align="left"><?php echo $obj->btnCancela(); ?></div>
    </div>  
    <section id="datatable-vertical-dynamic-height">
      <section>
        <table id="dtDynamicVerticalScrollExample" class="table table-striped table-bordered table-sm" cellspacing="0"
          width="100%">
          <thead>
            <tr>
              <th class="col-xs-2">#</th>
              <th class="col-xs-2"></th>
              <th class="col-xs-2">Cod. Cert.</th>
              <th class="col-xs-2">Id Alumno</th>
              <th class="col-xs-2">Nombre</th>
              <th class="col-xs-2">A.Paterno</th>
              <th class="col-xs-2">A.Materno</th>              
              <th class="col-xs-2">Tip.Certificado</th>
              <th class="col-xs-2">Estatus</th>            	
            </tr>
          </thead>
          <tbody>
			<?php		 
                  while ($data = sqlsrv_fetch_array($rQuery)) {    
                      echo '
                          <tr>
                            <td >'.$data["RW"].'</td>
                            <td>
           						<div class="form-check form-check-inline">
								  <input type="checkbox" class="form-check-input" id="'.$data["numeroControl"].'" style="visibility:hidden" value = "0">
								  <label class="form-check-label" id = "'.$data["numeroControl"].'" for="'.$data["numeroControl"].'"></label>
								</div>
                            </td>    
                            <td >'.$data["ID_UNICO_CERT"].'</td>
                            <td >P'.$data["numeroControl"].'</td>
                            <td >'.utf8_encode($data["nombre3"]).'</td>
                            <td >'.utf8_encode($data["primerApellido4"]).'</td>
                            <td >'.utf8_encode($data["segundoApellido5"]).'</td>
                            <td >'.utf8_encode($data["tipoCertificacion"]).'</td>                            
                            <td class="table-light"><input type="text" readonly class="form-control-plaintext" id="est'.$data["numeroControl"].'" value="'.$data["vp_estatus"].'" style="width: 60px;"></td>
                          </tr>
                      ';    
                  }
            ?>
          </tbody>
          <tfoot>
            <tr>
              <th>#</th>
              <th></th>
              <th>Cod.Cert</th>
              <th>Id Alumno</th>
              <th>Nombre</th>
              <th>A.Paterno</th>
              <th>A.Materno</th>
              <th>Tip.Certificado</th>
              <th>Estatus</th> 
            </tr>
          </tfoot>
        </table>
     </section>
    </section>
