<?php
session_start();
//echo $_SESSION['susu'];
if(isset($_SESSION['susu'])){
    require_once("util/utilerias.php");    
    echo "<h8 style='color:#0054a4;text-shadow: 5px 5px 5px #aaa; padding:20px 5px;'> Perfil de Usuario </h8>";	
    $obj = new Utilerias;
	$obj->CnnBD();
    $query = "SELECT * FROM reg_usu WHERE usu_nombre = '".$_SESSION['nom']."'";
    //echo $query;
    $rQuery = $obj->xQuery($query);
    while ($data = sqlsrv_fetch_array($rQuery)) {
    	echo "
    	<div style='width: 450px; margin-left: auto; margin-right: auto;'>
    		<div class='row'>
    			<div class='col-4'>Usuario</div>
    			<div class='col-4'><input type='text' id='txtUsuario' name='txtUsuario' size='30' value='".utf8_encode($data["usu_nombre"])."' disabled></div>
    		</div>
    		<br />
    		<div class='row'>
    			<div class='col-4'>Contraseña</div>
    			<div class='col-4'><input type='password' id='txtContra' name='txtContra' size='30' value='".utf8_encode($data["usu_contrasena"])."'></div>
    		</div>
    		<br />    
    		<div class='row'>
    			<div class='col-4'>Correo</div>
    			<div class='col-4'><input type='email' id='txtCorreo' name='txtCorreo' size='30' value='".utf8_encode($data["usu_correo"])."'></div>
    		</div>
    		<br />    
    		<button type='button' class='btn btn-primary btn-block' onclick='act()'>Actualizar</button>
    	</div>     
	   ";
    }       
}else{
    header("Location: index2.php");
}

?>
<script>
function act() {
    formdata = new FormData();    
    contra = $("#txtContra").val();
    correo = $("#txtCorreo").val();
    //alert('contr' + contra);
    formdata.append("contra", contra);
    formdata.append("correo", correo);
    jQuery.ajax({
        url: 'guarda.php',
        type: "POST",
        data: formdata,
        processData: false,
        contentType: false,
        success: function (result) {
            alert("Usuario actualizado correctamente");
            //$("#datatable" ).html( result );
        }
    });    
};    
</script>
