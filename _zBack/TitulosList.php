	<link rel='stylesheet' id='compiled.css-css'  href='./css/compiled-4.5.15.min.css' type='text/css' media='all' />
    <script type='text/javascript' src='./js/compiled.0.min.js?ver=4.5.15'></script>
	<script type='text/javascript' src='./js/tablax2.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
<script src="https://code.jquery.com/jquery-1.8.3.min.js"></script>
<script>
    function upfle(pname) {
        objname = '#up'+event.target.id;
        formdata = new FormData();
        file =$(pname)[0].files[0];
        xtitle = $(pname).attr("title");
        idAlumno = $('#IdAlumno').val();        
        formdata.append("file", file);
        formdata.append("idAlumno", idAlumno);
        formdata.append("tipoDoc", xtitle);
        jQuery.ajax({
            url: 'upload.php',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (result) {
                if(result == '0')
                {
                    $(objname).attr('src','./img/Uploadx.png');
                }else
                {
                    $(objname).attr('src','./img/Uploadb.png');   
                }
            }
        });
    } 
    function valida() {
        name = '#est'+$('#IdAlumno').val();
        $(name).val('V.Coordinacion');
        name = '#img'+$('#IdAlumno').val();
        $(name).removeClass('text-warning fa fa-exclamation').addClass('text-success fa fa-check');
    }
  
    $("#upfile1").click(function () {
        $("#file1").trigger('click');
    });
    $("#upfile2").click(function () {
        $("#file2").trigger('click');
    });
    $("#upfile3").click(function () {
        $("#file3").trigger('click');
    });
    $("#upfile4").click(function () {
        $("#file4").trigger('click');
    });
    $("#upfile5").click(function () {
        $("#file5").trigger('click');
    });
    $("#upfile6").click(function () {
        $("#file6").trigger('click');
    });
    $("#upfile7").click(function () {
        $("#file7").trigger('click');
    });
    $("#upfile8").click(function () {
        $("#file8").trigger('click');
    });
    $("#upfile9").click(function () {
        $("#file9").trigger('click');
    });
</script>



<?php
	require_once("util/utilerias.php");
	require_once("./config/xData.php");
	
    $obj = new Utilerias;
	$obj->CnnBD();
	
    $IDALUMNO = "001029309";
    $CVECARRERA = "611310";
    $CVECAMPUS = "090250";
    
    /*
    $vNumControl = $_GET['vNumControl'];
    $CARRERA = $_GET['carrera'];
    $CAMPUS = $_GET['campus'];
    */	
	//AND RC.PEOPLE_CODE_ID = 'P000008520'
	$NUMCONTROL = "AND RC.PEOPLE_CODE_ID = '".$vNumControl."'";
    $query = str_replace("VARIABLE=CAMPUS", $CAMPUS, CERTIFICADOSALTAS);
    $query = str_replace("VARIABLE=TIPO_CERTIFICADO", "AND (RC.TIPO_CERTIFICADO IN (".$TIPCERT."))", $query);
    $query = str_replace("VARIABLE=ANHO", $ANHO, $query);
    $query = str_replace("VARIABLE=FINICIAL", $FINICIO, $query);
    $query = str_replace("VARIABLE=FFINAL", $FFINAL, $query);
	$query = str_replace("VARIABLE=NUMCONTROL", $NUMCONTROL, $query);
    $query = str_replace("VARIABLE=VALPLANTEL", $VALPLANTEL, $query);
	
	//echo $query;
	
	$rQuery = $obj->xQuery($query);

	echo "
          <ul class='nav nav-tabs' role='tablist'>
            <li class='nav-item'><a class='nav-link active' data-toggle='tab' href='#GENERAL'>GENERAL</a></li>
            <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#IPES'>IPES</a></li>
            <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#RESPONSABLE'>RESPONSABLE</a></li>
            <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#RVOE'>RVOE</a></li>
            <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#CARRERA'>CARRERA</a></li>
            <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#ALUMNO'>ALUMNO</a></li>
            <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#EXPEDICION'>EXPEDICIÓN</a></li>
            <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#ASIGNATURAS'>ASIGNATURAS</a></li>
            <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#DOCUMENTOS'>DOCUMENTOS</a></li>
          </ul>
		  ";
		  
		  while ($data = sqlsrv_fetch_array($rQuery)) {
	echo "	  
		<div class='tab-content'>
			<div id='GENERAL' class='container tab-pane active'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Tip. Certificado</p></div>
					<div class='col-sm-6'>
						<input id='Tip. Certificado' type='text' autocomplete='false' value='".utf8_encode($data["tipoCertificado"])."' disabled/>		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Fol. Control</p></div>
					<div class='col-sm-6'>
						<input id='Fol. Control' type='text' autocomplete='false' value='".utf8_encode($data["folioControl"])."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Id Unico Certificado</p></div>
					<div class='col-sm-6'>
						<input id='Tip. Certificado' type='text' size='29' autocomplete='false' value='".utf8_encode($data["ID_UNICO_CERT"])."' disabled/>		
					</div>
				</div>                
			</div>
			<div id='IPES' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Nom. Institucíon</p></div>
					<div class='col-sm-6'>
						<input id='Nom. Institucíon' type='text' size='60' autocomplete='false' value='".utf8_encode($data["nombreInstitucion"])."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Campus</p></div>
					<div class='col-sm-6'>
						<input id='Campus' type='text' size='60' autocomplete='false' value='".utf8_encode($data["campus"])."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Ent. Federativa</p></div>
					<div class='col-sm-6'>
						<input id='Ent. Federativa' type='text' size='60' autocomplete='false' value='".utf8_encode($data["entidadFederativa"])."' disabled />		
					</div>
				</div>				
			</div>
            <div id='RESPONSABLE' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Nombre</p></div>
					<div class='col-sm-6'>
						<input id='Nombre' type='text' autocomplete='false' value='".utf8_encode($data["nombre"])."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>A. Paterno</p></div>
					<div class='col-sm-6'>
						<input id='A. Paterno' type='text' autocomplete='false' value='".utf8_encode($data["primerApellido"])."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>A. Materno</p></div>
					<div class='col-sm-6'>
						<input id='A. Materno' type='text' autocomplete='false' value='".utf8_encode($data["segundoApellido"])."' disabled />		
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Cargo</p></div>
					<div class='col-sm-6'>
						<input id='Cargo' type='text' size='60' autocomplete='false' value='".utf8_encode($data["cargo"])."' disabled />		
					</div>
				</div>			
            </div>
            <div id='RVOE' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Número</p></div>
					<div class='col-sm-6'>
						<input id='Número' type='text' autocomplete='false' value='".utf8_encode($data["claveCarrera"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Fec. Expedición </p></div>
					<div class='col-sm-6'>
						<input id='Fec. Expedición ' type='text' autocomplete='false' value='".utf8_encode($data["fecha"])."'  disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Clve. Plan</p></div>
					<div class='col-sm-6'>
						<input id='Clve. Plan' type='text' autocomplete='false' value='".utf8_encode($data["numeroControl"])."' disabled />
					</div>
				</div>
            </div>
            <div id='CARRERA' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Nom. Carrera</p></div>
					<div class='col-sm-6'>
						<input id='Nom. Carrera' type='text' autocomplete='false' value='".utf8_encode($data["nombreCarrera"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Tip. Periodo</p></div>
					<div class='col-sm-6'>
						<input id='Tip. Periodo' type='text' autocomplete='false' value='".utf8_encode($data["tipoPeriodo"])."' disabled />
					</div>
				</div>
            </div>
            <div id='ALUMNO' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>ID</p></div>
					<div class='col-sm-6'>
						<input id='IdAlumno' name='IdAlumno' type='text' autocomplete='false' value='".utf8_encode($data["numeroControl"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>CURP</p></div>
					<div class='col-sm-6'>
						<input id='CURP' type='text' autocomplete='false' value='".utf8_encode($data["curp2"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Nombre</p></div>
					<div class='col-sm-6'>
						<input id='Nombre' type='text' autocomplete='false' value='".utf8_encode($data["nombre3"])."' disabled />
					</div>
				</div>				
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>A. Paterno</p></div>
					<div class='col-sm-6'>
						<input id='A. Paterno' type='text' autocomplete='false' value='".utf8_encode($data["primerApellido4"])."' disabled />
					</div>
				</div>				
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>A. Materno</p></div>
					<div class='col-sm-6'>
						<input id='A. Materno' type='text' autocomplete='false' value='".utf8_encode($data["segundoApellido5"])."' disabled />
					</div>
				</div>				
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Genero</p></div>
					<div class='col-sm-6'>
						<input id='Id. Genero' type='text' autocomplete='false' value='".utf8_encode($data["idGenero"])."' disabled />
					</div>
				</div>				
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Fec. Nacimiento</p></div>
					<div class='col-sm-6'>
						<input id='Fec. Nacimiento' type='text' autocomplete='false' value='".utf8_encode($data["fechaNacimiento"])."' disabled />
					</div>
				</div>				
            </div>
            <div id='EXPEDICION' class='container tab-pane fade'><br>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Tip. Certificación</p></div>
					<div class='col-sm-6'>
						<input id='Tip. Certificación' type='text' autocomplete='false' value='".utf8_encode($data["tipoCertificacion"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Fecha</p></div>
					<div class='col-sm-6'>
						<input id='Fecha' type='text' autocomplete='false' value='".utf8_encode($data["fecha"])."' disabled />
					</div>
				</div>
				<div class='row'>				
					<div class='col-sm-3'><p class='navbar-text'>Lugar Espedición</p></div>
					<div class='col-sm-6'>
						<input id='Lugar Espedición' type='text' autocomplete='false' value='".utf8_encode($data["lugarExpedicion"])."' disabled />
					</div>
				</div>				
            </div>
            <div id='ASIGNATURAS' class='container tab-pane fade'><br>
                    <div class='tabla'>
                        <table id='grid' class='table table-hover dt-responsive nowrap '>
                            <thead>
                                <tr>
                                    <th>Clave</th>
                                    <th>Nombre</th>
                                    <th>Ciclo</th>
                                    <th>Calificación</th>
                                    <th>Observaciones</th>
                                </tr>
                            </thead>
                            <tbody>
	";	
								if($data["tipoCertificacion"] === 'TOTAL'){$TC = 'T';} else { $TC = 'P';}
								$queryA = str_replace("VARIABLE=PEOPLE_CODE_ID", $data["PEOPLE_CODE_ID"], CERTIFICADOSALTASMATERIAS);
								$queryA = str_replace("VARIABLE=CAMPUS", $data["ACADEMIC_SESSION"], $queryA);
								$queryA = str_replace("VARIABLE=TIPO_CERTIFICADO", $TC, $queryA);
								$queryA = str_replace("VARIABLE=ANHO", substr($data["fecha"], 0, 4), $queryA);
										
								//$queryA = str_replace("VARIABLE=ID_UNICO_CERT", $data["ID_UNICO_CERT"], $queryA);
								$rQueryA = $obj->xQuery($queryA);
								$TRegA = $obj->xCQuery();
	
								//echo $queryA;
								
								while ($datac = sqlsrv_fetch_array($rQueryA)) {
  		                            echo"<tr>";
        	                        echo"    <td>".utf8_encode($datac["claveAsignatura"])."</td>";
            	                    echo"    <td>".utf8_encode($datac["nombre6"])."</td>";
                	                echo"    <td>".utf8_encode($datac["ciclo"])."</td>";
                    	            echo"    <td>".utf8_encode($datac["calificacion"])."</td>";
                        	        echo"    <td>".utf8_encode($datac["observaciones"])."</td>";
                            	    echo"</tr>";
								}
								
	echo"														
                                <tr></tr>
                            </tbody>
                        </table>
                    </div>
            </div>                          
            <div id='DOCUMENTOS' class='container tab-pane fade'><br>
                <table id='grid' class='table dt-responsive nowrap '>
                    <thead>
                        <tr>
                            <th>Acta de Nacimiento</th>
                            <th>Historial Academico</th>
                            <th>Certificado Antecedente</th>
                            <th>Oficio de Validacion Antecedente Academico</th>
                            <th>CURP 200%</th>
                            <th>Equivalencia o Revalidación Estudios (Opcional)</th>
                            <th>Otros</th>
                            <th>Otros</th>
                            <th>Otros</th>
                        </tr>
                    </thead>
                    <form id='form' method='post' enctype='multipart/form-data'>
                    <tbody>
                        <tr>
                            <td>                          
                                <input type='file' id='file1' name='file1' style='display:none' title='Acta' onchange='upfle(this)' accept='application/pdf'/>
                                <img src='./img/Upload.png' id='upfile1' style='cursor:pointer' />
                                <div align='left' class='imagediv'> 
                                    <input id='selectfile' type='file' name='image' style='display: none;' onchange='upfle(this)' accept='application/pdf' />
                                </div>
                            </td>
                            <td>
                                <input type='file' id='file2' name='file2' style='display:none' title='H_Academico' onchange='upfle(this)' accept='application/pdf'/>
                                <img src='./img/Upload.png' id='upfile2' style='cursor:pointer' />
                                <div align='left' class='imagediv'> 
                                    <input id='selectfile' type='file' name='image' style='display: none;' />
                                </div>
                            </td>
                            <td>
                                <input type='file' id='file3' name='file3' style='display:none' title='CertificadoA' onchange='upfle(this)' accept='application/pdf'/>
                                <img src='./img/Upload.png' id='upfile3' style='cursor:pointer' />
                                <div align='left' class='imagediv'> 
                                    <input id='selectfile' type='file' name='image' style='display: none;' />
                                </div>
                            </td>                            
                            <td>
                                <input type='file' id='file4' name='file4' style='display:none' title='Ofi_Validacion' onchange='upfle(this)' accept='application/pdf'/>
                                <img src='./img/Upload.png' id='upfile4' style='cursor:pointer' />
                                <div align='left' class='imagediv'> 
                                    <input id='selectfile' type='file' name='image' style='display: none;' />
                                </div>
                            </td>                            
                            <td>
                                <input type='file' id='file5' name='file5' style='display:none' title='CURP' onchange='upfle(this)' accept='application/pdf'/>
                                <img src='./img/Upload.png' id='upfile5' style='cursor:pointer' />
                                <div align='left' class='imagediv'> 
                                    <input id='selectfile' type='file' name='image' style='display: none;' />
                                </div>
                            </td>                            
                            <td>
                                <input type='file' id='file6' name='file6' style='display:none' title='Equivalencia' onchange='upfle(this)' accept='application/pdf'/>
                                <img src='./img/Upload.png' id='upfile6' style='cursor:pointer' />
                                <div align='left' class='imagediv'> 
                                    <input id='selectfile' type='file' name='image' style='display: none;' />
                                </div>
                            </td>                            
                            <td>
                                <input type='file' id='file7' name='file7' style='display:none' title='Carta_Compromiso' onchange='upfle(this)' accept='application/pdf'/>
                                <img src='./img/Upload.png' id='upfile7' style='cursor:pointer' />
                                <div align='left' class='imagediv'> 
                                    <input id='selectfile' type='file' name='image' style='display: none;' />
                                </div>
                            </td>                            
                            <td>
                                <input type='file' id='file8' name='file8' style='display:none' title='Otros2' onchange='upfle(this)' accept='application/pdf'/>
                                <img src='./img/Upload.png' id='upfile8' style='cursor:pointer' />
                                <div align='left' class='imagediv'> 
                                    <input id='selectfile' type='file' name='image' style='display: none;' />
                                </div>
                            </td>                            
                            <td>
                                <input type='file' id='file9' name='file9' style='display:none' title='Otros3' onchange='upfle(this)' accept='application/pdf'/>
                                <img src='./img/Upload.png' id='upfile9' style='cursor:pointer' />
                                <div align='left' class='imagediv'> 
                                    <input id='selectfile' type='file' name='image' style='display: none;' />
                                </div>
                            </td>                            
                        </tr>
                        <tr>
                            <div id='result' name='result'></div>
                        </tr>
                    </tbody>
                    </form>
                </table>
            </div>
            <button type='button' class='btn btn-primary btn-block' onclick='valida()'>Validado</button>                             
		</div>        
	";
	}
?>