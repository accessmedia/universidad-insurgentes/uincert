<?php
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>


<script src="./js/jquery-1.3.2.min.js" type="text/javascript"></script>	

<div class="container">
  <table class="table table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th><input type="checkbox" name="chkAll" value="All"></th>
        <th>F.Control</th>
        <th>Nombre</th>
        <th>A.Paterno</th>
        <th>A.Materno</th>
        <th>Validar Info</th>
        <th>Estatus</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td><input type="checkbox" name="chkAll" value="54656453123"></td>
        <td>54656453123</td>
        <td>alejandro</td>
        <td>Jimkenez</td>
        <td>perez</td>
        <td><a href="#" id="1" title="1">Info Alumno</a></td>
        <td class="table-light">Prospecto</td>
      </tr>    
      <tr>
        <td>2</td>
        <td><input type="checkbox" name="chkAll" value="54656453123"></td>
        <td>54656453123</td>
        <td>alejandro</td>
        <td>Jimkenez</td>
        <td>perez</td>
		<td><a href="#" id="2" title="2">Info Alumno</a></td>
        <td class="table-danger">Prospecto</td>
      </tr>
            <tr>
        <td>3</td>
        <td><input type="checkbox" name="chkAll" value="54656453123"></td>
        <td>54656453123</td>
        <td>alejandro</td>
        <td>Jimkenez</td>
        <td>perez</td>
		<td><a href="#" id="3" title="3">Info Alumno</a></td>
        <td class="table-success">Prospecto</td>
      </tr>
      <tr>
        <td>4</td>
        <td><input type="checkbox" name="chkAll" value="54656453123"></td>
        <td>54656453123</td>
        <td>alejandro</td>
        <td>Jimkenez</td>
        <td>perez</td>
		<td><a href="#" id="4" title="4">Info Alumno</a></td>
        <td class="table-danger">Prospecto</td>
      </tr>
      <tr>
        <td>5</td>
        <td><input type="checkbox" name="chkAll" value="54656453123"></td>
        <td>54656453123</td>
        <td>alejandro</td>
        <td>Jimkenez</td>
        <td>perez</td>
		<td><a href="#" id="5" title="5">Info Alumno</a></td>
        <td class="table-danger">Faltan documentos</td>
      </tr>
    </tbody>
  </table>
</div>  

<script>


$(document).ready(function(){
	$("a").click(function(){		
		if(document.getElementById("fc").value == $(this).attr('title')){
			$("#ViewInfoAlumno").css("display","none");
		}else{
			$("#ViewInfoAlumno").css("display","block");
		}		
		document.getElementById("fc").value = $(this).attr('title');		
		/*
		$("#ViewInfoAlumno").each(function() {
			if($(this).css("display") == "block") {
				alert('none');
				$(this).css("display","none");
			} else {
				alert('block');
				$(this).css("display","block");
			}
			var href = $('#InfoAlumno').attr('title');
		    alert(href);
		});
		*/
	});
});
</script>
</head>


<div id="ViewInfoAlumno" style="display:none;">
		  <input id="fc" name="FolioControl" type="text" value="">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#GENERAL">GENERAL</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#IPES">IPES</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#RESPONSABLE">RESPONSABLE</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#RVOE">RVOE</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#CARRERA">CARRERA</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#ALUMNO">ALUMNO</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#EXPEDICION">EXPEDICIÓN</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#ASIGNATURAS">ASIGNATURAS</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#DOCUMENTOS">DOCUMENTOS</a></li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div id="GENERAL" class="container tab-pane active"><br>
                <p class="navbar-text">Tip. Certificado</p>
                <input id="Tip. Certificado" type="text" autocomplete="false" disabled/>
                
                <p class="navbar-text">Fol. Control</p>                            
                <input id="Fol. Control" type="text" autocomplete="false" disabled />                                                 
            </div>
            <div id="IPES" class="container tab-pane fade"><br>
                    <p class='navbar-text'>Nom. Institucíon</p>
                    <input id='Nom. Institucíon' type='text' autocomplete='false' disabled />
                    
                    <p class='navbar-text'>Campus</p>
                    <input id='Campus' type='text' autocomplete='false' disabled />
                    
                    <p class='navbar-text'>Ent. Federativa</p>
                    <input id='Ent. Federativa' type='text' autocomplete='false' disabled />
            </div>
            <div id="RESPONSABLE" class="container tab-pane fade"><br>
                <p class='navbar-text'>Nombre</p>
                <input id='Nombre' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>A. Paterno</p>
                <input id='A. Paterno' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>A. Materno</p>
                <input id='A. Materno' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Cargo</p>
                <input id='Cargo' type='text' autocomplete='false' disabled />
            </div>
            <div id="RVOE" class="container tab-pane fade"><br>
                <p class='navbar-text'>Número</p>
                <input id='Número' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Fec. Expedición </p>
                <input id='Fec. Expedición ' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Clve. Plan</p>
                <input id='Clve. Plan' type='text' autocomplete='false' disabled />
            </div>
            <div id="CARRERA" class="container tab-pane fade"><br>
                <p class='navbar-text'>Nom. Carrera</p>
                <input id='Nom. Carrera' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Tip. Periodo</p>
                <input id='Tip. Periodo' type='text' autocomplete='false' disabled />
            </div>
            <div id="ALUMNO" class="container tab-pane fade"><br>
                <p class='navbar-text'>ID</p>
                <input id='IdAlumno' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>CURP</p>
                <input id='CURP' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Nombre</p>
                <input id='Nombre' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>A. Paterno</p>
                <input id='A. Paterno' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>A. Materno</p>
                <input id='A. Materno' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Genero</p>
                <input id='Id. Genero' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Fec. Nacimiento</p>
                <input id='Fec. Nacimiento' type='text' autocomplete='false' disabled />
            </div>
            <div id="EXPEDICION" class="container tab-pane fade"><br>
                <p class='navbar-text'>Tip. Certificación</p>
                <input id='Tip. Certificación' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Fecha</p>
                <input id='Fecha' type='text' autocomplete='false' disabled />
                <p class='navbar-text'>Lugar Espedición</p>
                <input id='Lugar Espedición' type='text' autocomplete='false' disabled />
            </div>
            <div id="ASIGNATURAS" class="container tab-pane fade"><br>
                    <div class="tabla">
                        <table id="grid" class="table table-hover dt-responsive nowrap ">
                            <thead>
                                <tr>
                                    <th>Clave</th>
                                    <th>Nombre</th>
                                    <th>Ciclo</th>
                                    <th>Calificación</th>
                                    <th>Observaciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr></tr>

                            </tbody>
                        </table>
                    </div>
            </div>                          
            <div id="DOCUMENTOS" class="container tab-pane fade"><br>
                <table id="grid" class="table table-hover dt-responsive nowrap ">
                    <thead>
                        <tr>
                            <th>Acta de Nacimiento</th>
                            <th>Historial Academico</th>
                            <th>Certificado Antecedente</th>
                            <th>Oficio de Validacion Antecedente Academico</th>
                            <th>CURP 200%</th>
                            <th>Equivalencia o Revalidación Estudios (Opcional)</th>
                            <th>Carta Compromiso</th>
                            <th>Otros</th>
                            <th>Otros</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                        <tr></tr>
                    </tbody>
                </table>
            </div>                                                                                                                   
          </div>
          <div class="modal-footer">                    
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-success">Validado</button>
          </div>





</div>



