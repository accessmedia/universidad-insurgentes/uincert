<?php
if(isset($_SESSION['susu'])){
    require_once("util/utilerias.php");
    //https://www.w3schools.com/icons/fontawesome_icons_webapp.asp
?>
<!-- Navigation -->


<nav class="navbar navbar-inverse navbar-fixed-top bg-primary" style="min-height: 70px">
        <div class="container-fluid">

            <div class="navbar-header">
                <a class="navbar-brand" href="/" style="">
                    <img src="img/logo_uin.png" style="width: 200px">
                </a>
            </div>

<nav class="navbar navbar-expand-lg navbar-dark">

            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link cbfuente" href="index.php">
                        Inicio            
                    </a>
                </li>
                <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="2") OR ($_SESSION['rol']=="3")){?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        
                        Certificados                        
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="2")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('CertificadosPlantel', '');">
                            Plantel
                        </a>
                        <?php } ?>
                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="3")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('CertificadosCoordinacion', '');">
                            Coordinación
                        </a>  
                        <?php } ?>                      
                            <!--<a class="dropdown-item" href="javascript:cargaContenido('CertificadosCancelar', '');">
                            <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                            Cancelar
                        </a>   -->
                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="3")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'CERT_TIM');">
                            XML Timbrado
                        </a>
                        <?php } ?>  
                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="3")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'CERT_SEP');" target="_top">
                            XML SEP
                        </a>
                        <?php } ?>  
                    </div>
                </li>
                <?php } ?>
                <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="4") OR ($_SESSION['rol']=="5")){?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        
                        Títulos
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?php if (($_SESSION['rol']=="4")){?>

                        <a class="dropdown-item" href="javascript:cargaContenido('TitulosPlantel', '');">
                           
                            Validación
                        </a>
                        <?php } else if ( ($_SESSION['rol']=="5") OR ($_SESSION['rol']=="1") ) { ?>

                        <a class="dropdown-item" href="javascript:cargaContenido('TitulosPlantel', '');">
                            
                            Validación y XML
                        </a>

                        <?php } ?>

                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="5")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'TIT_TIM');">
                            
                            XML Timbrado
                        </a>
                        <?php } ?>
                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="5")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'TIT_SEP');" target="_top">
                            
                            XML SEP
                        </a>
                        <?php } ?>
                    </div>
                </li>
                <?php } ?>
            </ul>

            

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <ul class="navbar-nav mr-auto">
                     <?php if (($_SESSION['rol']!="0")){?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <!--  <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i> -->
                        Catálogos
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'entidades_federativas');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Entidades federativas 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'observaciones');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Observaciones 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'nivel_estudio');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Nivel de estudio 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'tipo_periodo');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Tipo de periodo 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'genero');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Genero 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'tipo_certificacion');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Tipo de certificacion 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'campus');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Campus 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'asignaturas');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Asignaturas 
                        </a>                                                                                                                                                                        
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'carreras');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Carreras 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'rvoe');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            RVOE 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'fundamento_social');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Fundamento legal serv social 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'mod_titulacion');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Modalidad titulacion 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'auth_recon');"> 
                            <i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Autorizacion o reconocimiento 
                        </a>                                                                                                                        
                    </div>
                </li> <?php } ?>

            </ul>

                
            <div class="collapse navbar-collapse" id="navbarSupportedContent">


        <ul class="navbar-nav mr-auto">
                    
                
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-top: -1%;">
                            <img class="rounded-circle" height="32px" src="img/img-user.png">
                            <span class="c"> Bienvenido
                            <?php if (($_SESSION['rol']!="0")){?>
                                <?php echo " " .$_SESSION['nom']." "?>
                             <?php } ?> !
                        </span>

                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <!-- <a class="dropdown-item" href="/">JOHANA TOVAR!</a> -->

                            <?php if (($_SESSION['rol']!="0")){?>
                            <a class="dropdown-item" href="javascript:cargaContenido('configusu', '');">
                                <!-- <i class="fa fa-user-circle-o" style="font-size: 15px; color:#0EA198;"></i> --> <?php echo " " .$_SESSION['nom']." "?>
                             <?php } ?>

                            <!-- <a class="dropdown-item" href="/Account/Profile">Ver Perfil</a> -->
                            <?php if (($_SESSION['rol']=="1")){?>
                                <a class="dropdown-item" href="javascript:cargaContenido('editarCuentas', '');">
                                    Editar usuario
                                 </a>
                            <?php } ?>
                            <!-- <a class="dropdown-item" href="/Manage/ChangePassword">Cambiar Contraseña</a> -->
                            <?php if (($_SESSION['rol']=="1")){?>
                             <a class="dropdown-item" href="javascript:cargaContenido('gcuentas', '');">
                                Alta usuario 
                             </a>
                <?php } ?>

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="logout.php">
                                    Cerrar Sesión
                                 </a>

                            <!-- <form method="post" id="logoutForm" class="login-header navbar-right" action="/Account/Logout">
                                <button type="submit" class="dropdown-item text-dark">Cerrar Sesión</button>                         
                            <input name="__RequestVerificationToken" type="hidden" value="CfDJ8C0dMl8PsRNCq_jfBSyKutpkXRsBuo8grATtjfd-7jWbN7Yn7PHtAdjshKVI8cLML2G89WZT9sPbQzZfN_hXRwXj6lWWob7L8ALWI8i5lbWEl_ajdozJbh9I_6K4PLVDL77Xuc5OnaiPfeXcf3irga9BUjs_TElRjux13mOhYuVd5GkSvDhQLGMt7W05Ek2BeQ"></form> -->
                        </div>
                    </li>
                </ul>
            </div>




            </nav>
        </div>
    </nav>

<nav class="navbar navbar-expand-lg navbar-light al-navbar">
    <div class="container" id = "containerMain" style="max-width: 100%; padding-right:0px; padding-left: 0px;">
        <!-- <a class="navbar-brand" href="index.php"><img src="./img/logo.png"></a> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primaryNav" aria-controls="primaryNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="primaryNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link cbfuente" href="index.php" style="font-family: Helvetica; color:#999; font-size: 12px;">
	                    <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
    	                Inicio<!--<span class="sr-only">(current)</span>-->            	
                    </a>
                </li>
                <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="2") OR ($_SESSION['rol']=="3")){?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-family: Helvetica; color:#999; font-size: 12px;">
   	                    <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                        Certificados                        
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="2")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('CertificadosPlantel', '');">
	                        <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
    	                    Plantel
                        </a>
                        <?php } ?>
                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="3")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('CertificadosCoordinacion', '');">
	                        <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
    	                    Coordinación
                        </a>  
                        <?php } ?>                      
                            <!--<a class="dropdown-item" href="javascript:cargaContenido('CertificadosCancelar', '');">
                            <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                            Cancelar
                        </a>   -->
                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="3")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'CERT_TIM');">
                            <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                            XML Timbrado
                        </a>
                        <?php } ?>  
                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="3")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'CERT_SEP');" target="_top">
                            <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                            XML SEP
                        </a>
                        <?php } ?>  
                    </div>
                </li>
                <?php } ?>
                <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="4") OR ($_SESSION['rol']=="5")){?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-family: Helvetica; color:#999; font-size: 12px;">
                        <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                        Títulos
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?php if (($_SESSION['rol']=="4")){?>

                        <a class="dropdown-item" href="javascript:cargaContenido('TitulosPlantel', '');">
                            <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                            Validación
                        </a>
                        <?php } else if ( ($_SESSION['rol']=="5") OR ($_SESSION['rol']=="1") ) { ?>

                        <a class="dropdown-item" href="javascript:cargaContenido('TitulosPlantel', '');">
                            <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                            Validación y XML
                        </a>

                        <?php } ?>

                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="5")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'TIT_TIM');">
                            <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                            XML Timbrado
                        </a>
                        <?php } ?>
                        <?php if (($_SESSION['rol']=="1") OR ($_SESSION['rol']=="5")){?>
                        <a class="dropdown-item" href="javascript:cargaContenido('GeneraXML', 'TIT_SEP');" target="_top">
                            <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                            XML SEP
                        </a>
                        <?php } ?>
                    </div>
                </li>
                <?php } ?>
            </ul>
            <ul class="navbar-nav ml-auto">
                <?php //if ($_SESSION['rol']=="Admin"){?>
             <?php if (($_SESSION['rol']!="0")){?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-family: Helvetica; color:#999; font-size: 12px;">
                    <i class="fa fa-database" style="font-size: 15px; color:#0EA198;"></i>
                        Catálogos
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'entidades_federativas');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Entidades federativas 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'observaciones');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Observaciones 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'nivel_estudio');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Nivel de estudio 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'tipo_periodo');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Tipo de periodo 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'genero');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Genero 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'tipo_certificacion');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Tipo de certificacion 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'campus');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Campus 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'asignaturas');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Asignaturas 
                        </a>                                                                                                                                                                        
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'carreras');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Carreras 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'rvoe');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            RVOE 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'fundamento_social');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Fundamento legal serv social 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'mod_titulacion');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Modalidad titulacion 
                        </a>
                        <a class="dropdown-item" href="javascript:cargaContenido('catalogos', 'auth_recon');"> 
                        	<i class="fa fa-institution" style="font-size: 15px; color:#0EA198;"></i> 
                            Autorizacion o reconocimiento 
                        </a>                                                                                                                        
                    </div>
                </li> <?php } ?>
            </ul>
            <div class="al-icons">
                <?php if (($_SESSION['rol']=="1")){?>
                <a href="javascript:cargaContenido('gcuentas', '');"style="font-family: Helvetica; color:#999; font-size: 12px;">
                	<i class="fa fa-users" style="font-size: 15px; color:#0EA198;"></i> 
                	Alta usuario 
               	</a>
                <?php } ?>
                <?php if (($_SESSION['rol']=="1")){?>
                <a href="javascript:cargaContenido('editarCuentas', '');"style="font-family: Helvetica; color:#999; font-size: 12px;">
                    <i class="fa fa-users" style="font-size: 15px; color:#0EA198;"></i> 
                    Editar usuario
                </a>
                <?php } ?>
                <!--<a href="#"><i class="fa fa-cog" style="font-size: 22px;"></i> Configuracion </a>-->
                <?php if (($_SESSION['rol']!="0")){?>
                <a href="javascript:cargaContenido('configusu', '');"style="font-family: Helvetica; color:#999; font-size: 12px;">
                	<i class="fa fa-user-circle-o" style="font-size: 15px; color:#0EA198;"></i> <?php echo " " .$_SESSION['nom']." "?>
                 <?php } ?>
                     </a>
                <a href="logout.php"><i class="fa fa-sign-out" style="font-size: 15px; color:#0EA198;"></i> Salir </a>
                <!--<a href="javascript:cargaContenido('logout', '');"><i class="fa fa-sign-out" style="font-size: 22px;"></i> Salir </a>-->
            </div>
        </div>
    </div>
</nav>

<?php
    }else{
        header("Location: index.php");
    }
?>



