<?php
    header('Content-Type: text/html; charset=UTF-8');
    
    $SecureSection = false;
    require_once("util/utilerias.php");
    require_once("./config/xData.php");

    $cont = 0;
    $obj = new Utilerias;
    $obj->CnnBD();

    echo '<h2 class="text-primary"> Generando XML de alumnos: </h2>';

    $CAMPUS             = $_GET['campusId'];
    $vNumControl        = $_GET['vNumControl'];
    $comma_separated    = EXPLODE(",", $vNumControl);

    $ZipFile            = "Tit_".date("Y")."-".date("m")."-".date("d")."-".date("H")."-".date("i")."-".date("s")."_".rand().".zip";
    $thisPath           = "./XMLsTitulos/General";
    $FilePath           = $_SERVER["PHP_SELF"];
    $FilePath           = str_replace("GenXMLT.php", "", $FilePath);

    $LinkFile           = 'http://'.$_SERVER["SERVER_NAME"].':'.$_SERVER["SERVER_PORT"].'/'.$FilePath.'/XMLsTitulos/General/'.$ZipFile;
    $FilePath           = $_SERVER['DOCUMENT_ROOT'].'/'.$FilePath.'/XMLsTitulos/General';
    
    $FilePath           = $thisPath.'/'.$ZipFile;

    if(file_exists($FilePath)){unlink($FilePath);}

    foreach($comma_separated as $key){
        $NUMCONTROL     = " std.PEOPLE_ID IN(".$key.")";
        $IDALUMNO       = substr($key, 1, 9);
        $CARRERA        = substr($key, 11, 6);

        $key2       = str_replace("'", "", $IDALUMNO);
        $query      = str_replace("VARIABLE=CAMPUS", $CAMPUS, TITULOS);
        $query      = str_replace("VARIABLE=CARRERA", $CARRERA, $query);
        $query      = str_replace("VARIABLE=PEOPLE_CODE_ID", $key2, $query);

        $query2     = "select * from Info_Validacion where folioControl = ".$IDALUMNO." AND cveCarrera = '".$CARRERA."'";
        $rQuery     = $obj->xQuery($query);
        $rQuery2    = $obj->xQuery($query2);

        while($data = sqlsrv_fetch_array($rQuery)){
            $thisVpUnicoCert    = "P".$data["folioControl"]."_".$CARRERA;
            $xquery             = "UPDATE REG_Validacion SET vp_estatus = 'Gen.XML' WHERE vp_id_unico_cert = '".$thisVpUnicoCert."'";        
            $obj->xQuery($xquery);

            $cont++;
            $sello = "";
            $certificadoResponsable = "";

            $data2              = sqlsrv_fetch_array($rQuery2);
            $xml                = new DomDocument('1.0', 'UTF-8');
            $xml->formatOutput  = true;
            $el_xml             = $xml->saveXML();

            if(!file_exists($thisPath)){mkdir($thisPath, 0777, true);}
            $filename = date("Y")."-".date("m")."-".date("d")."-".$data["folioControl"]."-Titulo-".$CARRERA.".xml";
            $xml->save(trim($filename));
            $fp = fopen($filename, 'w+');
            
            $thisStartDate  = ($data2["fechaInicio"]);
            $thisEndDate    = ($data2["fechaTerminacion"]);
            $thisExpDate    = ($data2["fechaExpedicion"]);
            $thisProfDate   = ($data2["fechaExamenProfesional"]);
            $thisExProfDate = ($data2["fechaExencionExamenProfesional"]);
            $thisStartDate2 = ($data2["fechaInicio2"]);
            $thisEndDate2   = ($data2["fechaTerminacion2"]);

            if($thisStartDate <> ""){$thisStartDate = $thisStartDate->format('Y-m-d');}
            if($thisEndDate <> ""){$thisEndDate = $thisEndDate->format('Y-m-d');}
            if($thisExpDate <> ""){$thisExpDate = $thisExpDate->format('Y-m-d');}
            if($thisProfDate <> ""){$thisProfDate = $thisProfDate->format('Y-m-d');}
            if($thisExProfDate <> ""){$thisExProfDate = $thisExProfDate->format('Y-m-d');}
            if($thisStartDate2 <> ""){$thisStartDate2 = $thisStartDate2->format('Y-m-d');}
            if($thisEndDate2 <> ""){$thisEndDate2 = $thisEndDate2->format('Y-m-d');}

            $thisStartDate  = ce2($thisStartDate);
            $thisEndDate    = ce2($thisEndDate);
            $thisExpDate    = ce2($thisExpDate);
            $thisProfDate   = ce2($thisProfDate);
            $thisExProfDate = ce2($thisExProfDate);
            $thisStartDate2 = ce2($thisStartDate2);
            $thisEndDate2   = ce2($thisEndDate2);

            if ((int)$data2["idEntidadFederativaExpedicion"] < 10){
                $idEntidadFederativaExpedicion = '0'.$data2["idEntidadFederativaExpedicion"];
            }else{
                 $idEntidadFederativaExpedicion = $data2["idEntidadFederativaExpedicion"];
            }

            if ((int)$data2["idEntidadFederativaAntecedente"] < 10){
                $idEntidadFederativaAntecedente = '0'.$data2["idEntidadFederativaAntecedente"];
            }else{
                $idEntidadFederativaAntecedente = $data2["idEntidadFederativaAntecedente"];
            }

            $newXml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'."\n";
            $newXml .= '<TituloElectronico xsi:schemaLocation="https://www.siged.sep.gob.mx/titulos/schema.xsd" xmlns="https://www.siged.sep.gob.mx/titulos/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.0" folioControl="'.utf8_encode($data["folioControl"]).'_'.utf8_encode($data["cveCarrera"]).'">'."\n";       
            $newXml .= '<FirmaResponsables>'."\n";
            
            $newXml .= " ".'  <FirmaResponsable nombre="'.utf8_encode($data["NOMBRE_RESP"]).'" primerApellido="'.utf8_encode($data["PRIMERAPELLIDO_RESP"]).'" segundoApellido="'.utf8_encode($data["SEGUNDOAPELLIDO_RESP"]).'" curp="'.utf8_encode($data["CURP_RESP"]).'" idCargo="'.utf8_encode($data["ID_CARGO"]).'" cargo="'.utf8_encode($data["CARGO"]).'" abrTitulo="'.utf8_encode($data["abrTitulo"]).'" sello="" certificadoResponsable="" noCertificadoResponsable="'.utf8_encode($data["noCertificadoResponsable"]).'"/>'."\n";
            $newXml .= " ".'  <FirmaResponsable nombre="'.utf8_encode($data["NOMBRE_RESP2"]).'" primerApellido="'.utf8_encode($data["PRIMERAPELLIDO_RESP2"]).'" segundoApellido="'.utf8_encode($data["SEGUNDOAPELLIDO_RESP2"]).'" curp="'.utf8_encode($data["CURP_RESP2"]).'" idCargo="'.utf8_encode($data["ID_CARGO2"]).'" cargo="'.utf8_encode($data["CARGO2"]).'" abrTitulo="'.utf8_encode($data["abrTitulo2"]).'" sello="" certificadoResponsable="" noCertificadoResponsable="'.utf8_encode($data["noCertificadoResponsable2"]).'"/>'."\n";        
            
            $newXml .= '</FirmaResponsables>'."\n";
            $newXml .= '  <Institucion cveInstitucion="'.utf8_encode($data["cvelInstitucion"]).'" nombreInstitucion="'.utf8_encode(ce($data["nombreInstitucion"])).'"/>'."\n";
            $newXml .= '  <Carrera cveCarrera="'.utf8_encode($data["cveCarrera"]).'" nombreCarrera="'.utf8_encode($data["nombreCarrera"]).'" fechaInicio="'.utf8_encode($thisStartDate).'" fechaTerminacion="'.utf8_encode($thisEndDate).'" idAutorizacionReconocimiento="'.$data2["idAutorizacionReconocimiento"].'" autorizacionReconocimiento="'.$data2["autorizacionReconocimiento"].'" numeroRvoe="'.utf8_encode($data["numeroRvoe"]).'"/>'."\n";
            $newXml .= '  <Profesionista curp="'.utf8_encode($data["curp"]).'" nombre="'.utf8_encode($data["nombre"]).'" primerApellido="'.utf8_encode($data["primerApellido"]).'" segundoApellido="'.utf8_encode($data["segundoApellido"]).'" correoElectronico="'.$data2["correoElectronico"].'"/>'."\n";
            $newXml .= '  <Expedicion fechaExpedicion="'.utf8_encode($thisExpDate).'" idModalidadTitulacion="'.$data2["idModalidadTitulacion"].'" modalidadTitulacion="'.$data2["modalidadTitulacion"].'" fechaExamenProfesional="'.utf8_encode($thisProfDate).'" fechaExencionExamenProfesional="'.utf8_encode($thisExProfDate).'" cumplioServicioSocial="'.$data2["cumplioServicioSocial"].'" idFundamentoLegalServicioSocial="'.$data2["idFundamentoLegalServicioSocial"].'" fundamentoLegalServicioSocial="'.$data2["fundamentoLegalServicioSocial"].'" idEntidadFederativa="'.$idEntidadFederativaExpedicion.'" entidadFederativa="'.$data2["entidadFederativaExpedicion"].'"/>'."\n";
            $newXml .= '  <Antecedente institucionProcedencia="'.$data2["institucionProcedencia"].'" idTipoEstudioAntecedente="'.$data2["idTipoEstudioAntecedente"].'" tipoEstudioAntecedente="'.$data2["tipoEstudioAntecedente"].'" idEntidadFederativa="'.$idEntidadFederativaAntecedente.'" entidadFederativa="'.$data2["entidadFederativaAntecedente"].'" fechaInicio="'.$thisStartDate2.'" fechaTerminacion="'.$thisEndDate2.'" noCedula="'.$data2["noCedula"].'"/>'."\n";
            $newXml .= '</TituloElectronico>'."\n";

            fwrite($fp, $newXml);
            fclose($fp);
        }
        zipFilesAndDownload($FilePath, $filename);
        unlink($filename);
    }
    
    function zipFilesAndDownload($thisFileName, $thisXMLFile){
        $zip = new ZipArchive();
        if($zip->open($thisFileName,ZIPARCHIVE::CREATE)===true){
            $zip->addFile($thisXMLFile);
            $zip->close();
            echo "Archivo .zip generado: <b>".$GLOBALS["ZipFile"]." - <a class='btn btn-success' href='".$GLOBALS["LinkFile"]."'><i class='fa fa-download'></i> DESCARGAR</a></b><br /><br />";
        }else{
            echo 'Error creando '.$thisFileName;
        }
    }

    function ce($s){
        $s = preg_replace("[&]","&amp;",$s);
        $s = preg_replace('["]',"&quot;",$s);
        //$s = preg_replace('["]',"'",$s);
        $s = preg_replace("[<]","&lt;",$s);
        $s = preg_replace("[>]","&gt;",$s);
        //$s = preg_replace("[']","&apos",$s);    
        $s = preg_replace("[áàâãª]","á",$s);
        $s = preg_replace("[ÁÀÂÃ]","Á",$s);
        $s = preg_replace("[éèê]","é",$s);
        $s = preg_replace("[ÉÈÊ]","É",$s);
        $s = preg_replace("[íìî]","í",$s);
        $s = preg_replace("[ÍÌÎ]","Í",$s);
        $s = preg_replace("[óòôõº]","ó",$s); 
        $s = preg_replace("[ÓÒÔÕ]","Ó",$s);
        $s = preg_replace("[úùû]","ú",$s);
        $s = preg_replace("[ÚÙÛ]","Ú",$s);
        $s = str_replace("ñ","ñ",$s);
        $s = str_replace("Ñ","Ñ",$s);
        $s = str_replace("Ñ","Ñ",$s);
        $s = str_replace("1900-01-01","",$s);
        //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
        //$s = str_replace("caracter-queremos-cambiar","caracter-por-el-cual-vamos-a-cambiar",$s);
        return $s;
    }

    function ce2($s){
        $s = str_replace("1900-01-01","",$s);
        //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
        //$s = str_replace("caracter-queremos-cambiar","caracter-por-el-cual-vamos-a-cambiar",$s);
        return $s;
    }

    function ceComilla($s){
        $s = preg_replace('["]',"'",$s);
        //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
        //$s = str_replace("caracter-queremos-cambiar","caracter-por-el-cual-vamos-a-cambiar",$s);
        return $s;
    }
?>
