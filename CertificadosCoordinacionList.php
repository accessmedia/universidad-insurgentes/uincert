<?php
header('Content-Type: text/html; charset=UTF-8');
?>

<?php
$SecureSection = false;
require_once("util/utilerias.php");
$obj = new Utilerias;
$obj->CnnBD();

$CAMPUS = $_POST['campus'];
$TIPCERT = $_POST['tipCert'];
$ANHO = $_POST['anho'];
$FINICIO = $_POST['finicial'];
$FFINAL = $_POST['ffinal'];

$VALPLANTEL = "INNER JOIN REG_Validacion RVP ON RVP.VP_ID_UNICO_CERT = T1.PEOPLE_CODE_ID+T1.folioControl+T1.Campus  where vp_estatus IN ('Gen.XML','Validado','Validado por CSE','Rechazado por CSE','XML Timbrado','XML Autenticado') ";

$query = str_replace("VARIABLE=CAMPUS", $CAMPUS, CERTIFICADOSHEADER);
$query = str_replace("VARIABLE=TIPO_CERTIFICADO", "AND (RC.TIPO_CERTIFICADO IN (" . $TIPCERT . "))", $query);
$query = str_replace("VARIABLE=ANHO", $ANHO, $query);
$query = str_replace("VARIABLE=FINICIAL", $FINICIO, $query);
$query = str_replace("VARIABLE=FFINAL", $FFINAL, $query);
$query = str_replace("VARIABLE=NUMCONTROL", "", $query);
$query = str_replace("VARIABLE=VALPLANTEL", $VALPLANTEL, $query);

//echo "Query <br>".$query; 
?>

<!--<link rel='stylesheet' id='compiled.css-css'  href='./css/compiled-4.5.15.min.css' type='text/css' media='all' />-->
<!--<script type='text/javascript' src='./js/compiled.0.min.js?ver=4.5.15'></script>-->
<!--<script type='text/javascript' src='./js/tablax2.js'></script>-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<script>
    // Datatable vertical dynamic height
    $(document).ready(function () {

        $("#dtDynamicVerticalScrollExample").dataTable().fnDestroy();
        $.fn.dataTable.ext.errMode = 'none';

        $('#dtDynamicVerticalScrollExample').DataTable({
            "bDestroy": true,
            "scrollY": "45vh",
            "scrollCollapse": true,
            "paging": false
        });

        $('.dataTables_length').addClass('bs-select');

        $(".btn1").click(function () {
            var vNumControl = $(this).parents("tr").find("td")[2].innerHTML;
            var campus = $("#cmbCampus option:selected").val();


            var finicio = $('#finicio').val();
            var ffinal = $('#ffin').val();
            var vNumControl = vNumControl;
            var anho = $(this).attr('year');
            var curiculum = $('#txt' + vNumControl).val();
            xtipCert = $("#cmbTipCertificado option:selected").text();
            if (xtipCert == "Ambos") {
                tipCert = "'T','P'";
            } else {
                if (xtipCert == "Total") {
                    tipCert = "'T'";
                } else {
                    tipCert = "'P'";
                }
            }
            $("#ViewInfoAlumno").text("");
            var res = campus.replace(" ", "%20");

            // $("#ViewInfoAlumno").load('infoAlumnoC.php?campus=' + res
            //     + '&anho=' + anho
            //     + '&finicio=' + finicio
            //     + '&ffinal=' + ffinal
            //     + '&vNumControl=' + vNumControl
            //     + '&curiculum=' + curiculum
            //     + '&tipCert=' + tipCert
            // );

            // $("html, body").animate({scrollTop: $(document).height()}, 1000);

            $('#infoModalTitle').html('Detalle del estudiante');
            $("#infoModalBody").load('infoAlumnoC.php?campus=' + res
                + '&anho=' + anho
                + '&finicio=' + finicio
                + '&ffinal=' + ffinal
                + '&vNumControl=' + vNumControl
                + '&curiculum=' + curiculum
                + '&tipCert=' + tipCert
            );
            $("#infoModalBody").empty();
            $('#infoModal').modal();

        });

        $(".btn2").click(function () {
            var valores = $(this).parents("tr").find("td")[3].innerHTML;
            console.log(valores);
            //alert(valores);
        });

        $(".form-check-input").click(function (e) {
            e.stopImmediatePropagation();
            var vNumControl = $(this).parents("tr").find("td")[2].innerHTML;
            var campo = '#txt' + vNumControl;

            campo = campo.replace("'", "");
            campo = campo.replace("'", "");
            campo = campo.replace(",", "");

            var thisYear = '#Year' + vNumControl;
            thisYear = thisYear.replace("'", "");
            thisYear = thisYear.replace("'", "");
            thisYear = thisYear.replace(",", "");

            if ($("#txtGenXML").val().indexOf(vNumControl) != -1) {
                vYearXML = $("#txtYear").val();
                vlCurXML = $("#txtCur").val();
                vlCadXML = $("#txtGenXML").val();

                vYearXML = vYearXML.replace(", '" + $(thisYear).val() + "'", "");
                vlCurXML = vlCurXML.replace(", '" + $(campo).val() + "'", "");
                vlCadXML = vlCadXML.replace(",'" + vNumControl + "'", "");
            } else {
                vYearXML = $("#txtYear").val() + ",'" + $(thisYear).val() + "'";
                vlCurXML = $("#txtCur").val() + ",'" + $(campo).val() + "'";
                vlCadXML = $("#txtGenXML").val() + ",'" + vNumControl + "'";
            }

            $("#txtYear").val(vYearXML);
            $("#txtCur").val(vlCurXML);
            $("#txtGenXML").val(vlCadXML);

            $("#ViewInfoAlumno").html("");
        });

        $("#btnGenXML").click(function (e) {
            e.stopImmediatePropagation();
            var vNumControl = $('#txtGenXML').val();
            var curiculum = $('#txtCur').val();
            var vYear = $('#txtYear').val();

            if (vNumControl.length == 0) {
                alert('Por favor seleccione al menos un alumno antes de generar un XML');
            } else {
                var campusFolder = $("#cmbCampus option:selected").text();
                var campus = $("#cmbCampus option:selected").val();
                var res = campus.replace(" ", "%20");
                var resFolder = campusFolder.replace(" ", "%20");
                var campusDescriptionCert = resFolder;

                var finicio = $('#finicio').val();
                var ffinal = $('#ffin').val();
                var anho = vYear.substring(1, 10000);
                var vNumControl = vNumControl.substring(1, 10000);
                var curiculum = curiculum.substring(1, 10000);

                xtipCert = $("#cmbTipCertificado option:selected").text();
                if (xtipCert == "Ambos") {
                    tipCert = "'T','P'";
                } else {
                    if (xtipCert == "Total") {
                        tipCert = "'T'";
                    } else {
                        tipCert = "'P'";
                    }
                }

                $("#ViewInfoAlumno").text("");
                $("html, body").animate({scrollTop: $(document).height()}, 1000);

                var res = res.replace(" ", "%20");
                $("#ViewInfoAlumno").load('GenXML.php?campus=' + res
                    + '&anho=' + anho
                    + '&finicio=' + finicio
                    + '&ffinal=' + ffinal
                    + '&vNumControl=' + vNumControl
                    + '&curiculum=' + curiculum
                    + '&tipCert=' + tipCert
                    + '&campusDescriptionCert=' + campusDescriptionCert
                );
                vlCadXML.empty();
            }
        });
    });

    $(document).ready(function () {
        $(".DisplayType").click(function () {
            var thisValue = $(this).val();
            if ($(this).prop('checked')) {
                $(".Alumno_" + thisValue).show();
            } else {
                $(".Alumno_" + thisValue).hide();
            }
        });
    })
</script>


<?php
$rQuery = $obj->xQuery($query);
?>

<div class="row">
    <div class="col"></div>
    <div class="col"></div>
    <div class="col"><input type="text" id="txtYear" name="txtYear" style="visibility: hidden;"></div>
    <div class="col"><input type="text" id="txtCur" name="txtCur" style="visibility: hidden;"></div>
    <div class="col"><input type="text" id="txtGenXML" name="txtGenXML" style="visibility: hidden;"></div>
    <div id='XmlGen' class="col" align="left">
        <button type='submit' id='btnGenXML' class='btn btn-success' style='width:235px; padding:8px;'>
            <i class='fa fa-file-excel-o'></i>
            <span>Generar XML</span>
        </button>
    </div>
</div>
<section id="datatable-vertical-dynamic-height">
    <section>
<!--        <div style="position:relative; width:100%; height:70px;">-->
<!--            <div style="position:absolute; width:100%; text-align:center;">-->
<!--                <input type="checkbox" name="DisplayType" class="DisplayType" value="Rechazado"-->
<!--                       style="pointer-events:auto; margin-top:5px;" checked> <span style='margin-left:15px'>Pendiente por CSE |</span>-->
<!--                <input type="checkbox" name="DisplayType" class="DisplayType" value="Validado"-->
<!--                       style="pointer-events:auto; margin-top:5px;" checked> <span style='margin-left:15px'>Validado por CSE</span>-->
<!--            </div>-->
<!--        </div>-->

        <div class="text-center" style="position:relative; width:100%;">
            <label class="container-checkbox mr-2">Pendiente por CSE
                <input type="checkbox" name="DisplayType"  class="DisplayType" value="Rechazado" checked>
                <span class="checkmark"></span>
            </label>
            <label class="container-checkbox ">Validado por CSE
                <input type="checkbox" name="DisplayType"  class="DisplayType" value="Validado" checked>
                <span class="checkmark"></span>
            </label>
        </div>

        <div class="accordion mb-2 " id="accordionExample">
            <div class="card border-0">
                <div class="card-header p-1 px-2 border-0 bg-white rounded-top text-white bb-style-1" id="headingThree">
                    <h6 class=" float-left  text-primary">
                        Notas:

                    </h6>
                    <a class="btn btn-link float-right border-0 p-0 border-0" data-toggle="collapse"
                       data-target="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                        <i class="fa fa-angle-down text-primary"></i>
                    </a>
                </div>
                <div id="collapseThree" class="collapse show" aria-labelledby="headingThree"
                     data-parent="#accordionExample" style="">
                    <div class="card-body p-0 pt-2">
                        <ol>
                            <li>
                                <h6>Haga click sobre el icono: <i class="text-info fa fa-id-card-o"></i> para ver o editar la informaci&oacute;n
                                    de cada alumno
                                </h6>
                            </li>
                            <li>
                                <h6>Haga click sobre el icono: <i class="text-warning fa fa-user-times"></i> para ver la Raz&oacute;n
                                    del por que se rechaz&oacute; cada alumno
                                </h6>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div id="pills-tabContent" class="tab-content">

            <div class="col-md-12 left">
                <div class="container col-lg-12">
                    <table id="dtDynamicVerticalScrollExample" class="table table-rounded table-striped table-sm"
                           cellspacing="0" width="100%" class="bg-primary rounded-top text-white"
                           style="text-align:center;">
                        <thead>
                        <tr class="bg-primary rounded-top text-white"
                            style='text-align:center; background-color:#67a1fc;'>
                            <th class="col-xs-2">#</th>
                            <th class="col-xs-2">Obtener XML</th>
                            <th class="col-xs-2">Id Alumno</th>
                            <th class="col-xs-2">Nombre</th>
                            <th class="col-xs-2">A.Paterno</th>
                            <th class="col-xs-2">A.Materno</th>
                            <th class="col-xs-2">Tip.Certificado</th>
                            <th class="col-xs-2">Validar Info</th>
                            <th class="col-xs-2">Estatus</th>
                            <th class="col-xs-2">Raz&oacute;n</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        while ($data = sqlsrv_fetch_array($rQuery)) {
                            if (($data["vp_estatus"] === 'Validado por CSE') OR ($data["vp_estatus"] === 'Gen.XML')) {
                                $ClassStatus = "Prospecto";
                                if ($data["vp_estatus"] === 'Gen.XML') {
                                    $check = 'XML Generado';
                                } else {
//                                    $check = '
//                <div class="form-check form-check-inline">
//                  <input type="checkbox" class="form-check-input" id="' . $data["numeroControl"] . '" style="visibility:hidden" value = "0">
//                  <label class="form-check-label" id = "' . $data["numeroControl"] . '" for="' . $data["numeroControl"] . '"></label>
//                </div>
//              ';
                                    $check = '<label class="container-checkbox form-check form-check-inline">
                <input type="checkbox" name="DisplayType"  class="DisplayType form-check-input" id="' . $data["numeroControl"] . '" style="visibility:hidden" value = "0">
                <span class="checkmark form-check-label"  id = "' . $data["numeroControl"] . '" for="' . $data["numeroControl"] . '"></span>
            </label>
              ';
                                }
                            } else {
                                $check = '';
                            }

                            if (($data["vp_estatus"] === 'Validado por CSE') OR ($data["vp_estatus"] === 'Gen.XML') OR ($data["vp_estatus"] === 'XML autenticado') OR ($data["vp_estatus"] === 'XML timbrado')) {
                                $ClassStatus = "Validado";
                                $image = '<i class="text-success fa fa-check" id="img' . $data["numeroControl"] . '"></i>  ';
                            } else {

                                $ClassStatus = "Rechazado";
                                $image = '<i class="text-warning fa fa-exclamation" id="img' . $data["numeroControl"] . '"></i>  ';
                            }

                            $DeniedReason = $obj->getDbRowName("vp_razon_rechazo", "Reg_Validacion", "WHERE vp_id_unico_cert = '" . $data["vp_id_unico_cert"] . "'", 0);

                            if ($DeniedReason <> "") {
                                $DeniedButton = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-fancybox='VerRazonRechazo' data-src='#VerRazonRechazo" . $data["numeroControl"] . "' href='javascript:;'><i class='text-warning fa fa-user-times'></i></a>";
                            } else {
                                $DeniedButton = "N/A";
                            }

                            echo '
         <tr class="Alumno_' . $ClassStatus . '">
         <td >' . $data["RW"] . '</td>
         <td id="CheckAlumno_P' . $data["numeroControl"] . '">' . $check . '</td>
         <td >P' . $data["numeroControl"] . '</td>
         <td >' . utf8_encode($data["nombre3"]) . '</td>
         <td >' . utf8_encode($data["primerApellido4"]) . '</td>
         <td >' . utf8_encode($data["segundoApellido5"]) . '</td>
         <td >' . utf8_encode($data["tipoCertificacion"]) . '</td>
         <td id="' . $data["numeroControl"] . '" title="' . $data["numeroControl"] . '">
         <!--<button class="btn btn-outline-primary btn2"><i class="fa fa-id-card-o"></i></button>-->
         <button class="btn btn-outline-primary btn1" value=' . $data["numeroControl"] . ' year=' . $data["ANHO"] . '><i class="fa fa-id-card-o"></i></button>
         ' . $image . '                              
         <input type="text" readonly id="txtP' . $data["numeroControl"] . '" value=' . $data["curriculum"] . ' style="width: 1px; visibility: hidden;">
         <input type="text" readonly id="YearP' . $data["numeroControl"] . '" value=' . $data["ANHO"] . ' style="width: 1px; visibility: hidden;">
         </td>
         <td><input type="text" readonly class="form-control-plaintext" id="est' . $data["numeroControl"] . '" value="' . $data["vp_estatus"] . '" style="text-align:center;"></td>
         <td id="raz_P' . $data["numeroControl"] . '" style="text-align:center;">' . $DeniedButton . '</td>
         </tr>
         ';
                            if (strlen($DeniedReason) <> 0) {
                                echo '<span id="VerRazonRechazo' . $data["numeroControl"] . '" style="width:600px;" class="fancy_info">
              <b>Raz&oacute;n de Rechazo del Alumno:</b><br />' . utf8_encode($data["nombre3"]) . ' ' . utf8_encode($data["primerApellido4"]) . ' ' . utf8_encode($data["segundoApellido5"]) . '
              <hr style="color: #0056b2;" />
              <p>' . $DeniedReason . '</p>
          </span>';
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                        <tr class="bg-primary rounded-top text-white">
                            <th>#</th>
                            <th></th>
                            <th>Id Alumno</th>
                            <th>Nombre</th>
                            <th>A.Paterno</th>
                            <th>A.Materno</th>
                            <th>Tip.Certificado</th>
                            <th>Validar Info</th>
                            <th>Estatus</th>
                            <th>Raz&oacute;n</th>
                        </tr>
                        </tfoot>
                </div>
            </div>
        </div>
        </table>
    </section>
</section>
