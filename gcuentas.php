﻿
<?php
    session_start();
    $SecureSection = false;
    require_once("util/utilerias.php");
    $obj = new Utilerias;
    $obj->CnnBD();

    switch ($_SESSION["SectionType"]) {
        case 'Servicios_Profesionales':
            $thisPath = "../";
        break;
        default:
            $thisPath = "";
        break;
    }
?>

<script>
    $(document).ready(function() { 
        $("#txtUsuario").attr('value', '');   
        $("#txtContra").attr('value', '');
        $("#txtCorreo").attr('value', '');
        $("#cmbCampus").attr('value', '');
        $("#txtRol").attr('value', '');
    });

    function valida(){
        formdata = new FormData(); 
        usuario = $("#txtUsuario").val();   
        contra = $("#txtContra").val();
        correo = $("#txtCorreo").val();
        campus = $("#cmbCampus").val();
        rol = $("#txtRol").val();

        if ($("#auth_ss").is(':checked')) {auth_ss = 1;}else{auth_ss = 0;}
        if ($("#auth_pp").is(':checked')) {auth_pp = 1;}else{auth_pp = 0;}
        if ($("#auth_ti").is(':checked')) {auth_ti = 1;}else{auth_ti = 0;}
        if ($("#auth_co").is(':checked')) {auth_co = 1;}else{auth_co = 0;}

        if ((usuario == '') || (contra == '') || (correo == '')  )
        {
             alert("Por favor llena todos los campos");
        }
        else
        {
        formdata.append("usuario", usuario);
        formdata.append("contra", contra);
        formdata.append("correo", correo);
        formdata.append("cmbCampus", campus);
        formdata.append("rol", rol);

        formdata.append("auth_ss", auth_ss);
        formdata.append("auth_pp", auth_pp);
        formdata.append("auth_ti", auth_ti);
        formdata.append("auth_co", auth_co);

            jQuery.ajax({
            url: '<?php echo $thisPath;?>insertarNuevoUsuario.php',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (result) {
                if (result == 1)
                {
                    alert("Usuario agregado correctamente");
                    $("#txtUsuario").val('');
                    $("#txtContra").val('');
                    $("#txtCorreo").val('');
                    $("#cmbCampus").val('');
                
                    $("#auth_ss").val('');
                    $("#auth_pp").val('');
                    $("#auth_ti").val('');
                    $("#auth_co").val('');
                }
                else
                {
                    alert("El usuario que intenta ingresar ya existe");
                }
            }
        });
        }
    }

    $(document).ready(function(){
        var thisRol = $("#txtRol").val();
        $("#txtRol").click(function(){
            if($("#txtRol").val() == 1 || $("#txtRol").val() == 3 || $("#txtRol").val() == 4 || $("#txtRol").val() == 5){
                $("#cmbCampus").val('');
                $("#cmbCampus").prop('disabled', true);
            }else{
                $("#cmbCampus").prop('disabled', false);
            }
        });
    })
</script>

<script>
    document.getElementById('cmbCampus').value='';
</script>

<h2 class="text-primary">ALTA DE USUARIO</h2>
<div class="text-center" style="background-color:#f1f2f3; width: 70%; padding:20px; margin-left: auto; margin-right: auto;">
    <div class='row'>
        <div class='col-4'><h5 class="text-left">Usuario</h5></div>
        <div class='col-8'><input type="text" id='txtUsuario' name='txtUsuario' class="form-control" placeholder="Ingresar usuario"></div>
    </div>
    <br />
    <div class='row'>
        <div class='col-4'><h5 class="text-left">Contraseña</h5></div>
        <div class='col-8'><input type="password" id='txtContra' name='txtContra' class="form-control" placeholder="Ingresar contraseña"></div>

    </div>
    <br />    
    <div class='row'>
        <div class='col-4'><h5 class="text-left">Correo</h5></div>
        <div class='col-8'><input id='txtCorreo' name='txtCorreo' class="form-control" placeholder="Ingresar correo" type="email"></div>
    </div>
    <br />
    <div class='row'>
        <div class='col-4'><h5 class="text-left">Campus</h5></div>
        <div class='col-8'><?php echo $obj->cmbCampus(); ?></div>
    </div>
    <br />
    <div class='row'>
        <div class='col-4'><h5 class="text-left">Rol</h5></div>
        <div class='col-8'>
            <select class='form-control' id='txtRol' name='txtRol' >
                <option value="0">Inactivo</option>
                <option value="1">Administrador</option>
                <option value="2">Usuario certificado plantel</option>
                <option value="3">Usuario certificado CSE</option>
                <option value="4">Usuario títulos alta</option>
                <option value="5">Usuario títulos administrador</option> 
            </select>
         </div>
    </div>
    <br /><br />
    <div id='PermisosAdicionales' class='row'>
        <div class='col-12' style='text-align:left;'><h4>Permisos Adicionales</h4></div>
        <div class='row' style='width:100%; justify-content:center; margin-top:20px; margin-bottom:20px;'>
            <div class='col-2'>
                <h6 class='card-title'>Servicio Social</h6>
                <input type='checkbox' class='form-check-input' id='auth_ss' name='auth_ss' value='' style='width:20px; height:20px; margin:auto;'>
            </div>
            <div class='col-4'>
                <h6 class='card-title'>&nbsp;&nbsp;&nbsp;&nbsp;Pr&aacute;cticas Profesionales</h6>
                <input type='checkbox' class='form-check-input' id='auth_pp' name='auth_pp' value='' style='width:20px; height:20px; margin:auto;'>
            </div>
            <div class='col-2'>
                <h6 class='card-title'>&nbsp;&nbsp;&nbsp;&nbsp;Titulaci&oacute;n</h6>
                <input type='checkbox' class='form-check-input' id='auth_ti' name='auth_ti' value='' style='width:20px; height:20px; margin:auto;'>
            </div>
            <div class='col-2'>
                <h6 class='card-title'>&nbsp;&nbsp;&nbsp;&nbsp;Convenios</h6>
                <input type='checkbox' class='form-check-input' id='auth_co' name='auth_co' value='' style='width:20px; height:20px; margin:auto;'>
            </div>
        </div>
    </div>

    <br /><br />

    <div class='col-12' style='text-align:center;'>
        <button type='button' class='btn btn-success bg-success btn-sm' onclick='valida()' style=''><i class='fa fa-save' aria-hidden='true'></i> Crear Usuario</button>
    </div>
</div>
