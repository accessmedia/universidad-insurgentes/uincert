USE [Campus]
GO
/****** Object:  Table [dbo].[reg_acccampus]    Script Date: 19/12/2018 12:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reg_acccampus](
	[id_usu] [int] NULL,
	[id_campus] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[reg_campus]    Script Date: 19/12/2018 12:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reg_campus](
	[id_campus] [int] IDENTITY(1,1) NOT NULL,
	[campus_nom] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[reg_rol]    Script Date: 19/12/2018 12:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reg_rol](
	[id_rol] [int] NULL,
	[rol_nombre] [varchar](50) NULL,
	[rol_decripcion] [varchar](50) NULL,
	[rol_opc1] [int] NULL,
	[rol_opc2] [int] NULL,
	[rol_opc3] [int] NULL,
	[rol_opc4] [int] NULL,
	[rol_opc5] [int] NULL,
	[rol_opc6] [int] NULL,
	[rol_opc7] [int] NULL,
	[rol_opc8] [int] NULL,
	[rol_opc9] [int] NULL,
	[rol_opc10] [int] NULL,
	[rol_opc11] [int] NULL,
	[rol_opc12] [int] NULL,
	[rol_opc13] [int] NULL,
	[rol_opc14] [int] NULL,
	[rol_opc15] [int] NULL,
	[rol_opc16] [int] NULL,
	[rol_opc17] [int] NULL,
	[rol_opc18] [int] NULL,
	[rol_opc19] [int] NULL,
	[rol_opc20] [int] NULL,
	[rol_opc21] [int] NULL,
	[rol_opc22] [int] NULL,
	[rol_opc23] [int] NULL,
	[rol_opc24] [int] NULL,
	[rol_opc25] [int] NULL,
	[rol_opc26] [int] NULL,
	[rol_opc27] [int] NULL,
	[rol_opc28] [int] NULL,
	[rol_opc29] [int] NULL,
	[rol_opc30] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[REG_TITULOS]    Script Date: 19/12/2018 12:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REG_TITULOS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[version] [nvarchar](10) NULL,
	[foliocontrol] [nvarchar](10) NULL,
	[curp] [nvarchar](20) NULL,
	[idCargo] [nvarchar](10) NULL,
	[cargo] [nvarchar](50) NULL,
	[abrTitulo] [nvarchar](10) NULL,
	[Nombre] [nvarchar](50) NULL,
	[primerApellido] [nvarchar](50) NULL,
	[segundoApellido] [nvarchar](50) NULL,
	[sello] [nvarchar](max) NULL,
	[certificadoResponsable] [nvarchar](max) NULL,
	[NoCertificadoResponsable] [nvarchar](50) NULL,
	[curp2] [nvarchar](20) NULL,
	[idCargo2] [nvarchar](10) NULL,
	[cargo2] [nvarchar](50) NULL,
	[abrTitulo2] [nvarchar](10) NULL,
	[Nombre2] [nvarchar](50) NULL,
	[primerApellido2] [nvarchar](50) NULL,
	[segundoApellido2] [nvarchar](50) NULL,
	[sello2] [nvarchar](max) NULL,
	[certificadoResponsable2] [nvarchar](max) NULL,
	[NoCertificadoResponsable2] [nvarchar](50) NULL,
	[cveInstitucion] [nvarchar](6) NULL,
	[nombreInstitucion] [nvarchar](max) NULL,
	[cveCarrera] [nvarchar](10) NULL,
	[nombreCarrera] [nvarchar](max) NULL,
	[fechaInicio] [nvarchar](20) NULL,
	[fechaTerminacion] [nvarchar](20) NULL,
	[idAutorizacionreconocimiento] [nvarchar](5) NULL,
	[autorizacionReconocimiento] [nvarchar](50) NULL,
	[numeroRvoe] [nvarchar](50) NULL,
	[curp3] [nvarchar](20) NULL,
	[nombre3] [nvarchar](50) NULL,
	[primerApellido3] [nvarchar](50) NULL,
	[segundoApellido3] [nvarchar](50) NULL,
	[correoElectronico] [nvarchar](50) NULL,
	[fechaExpedicion] [nvarchar](20) NULL,
	[idModalidadTitulacion] [nvarchar](5) NULL,
	[modalidadTitulacion] [nvarchar](50) NULL,
	[fechaExamenProfesional] [nvarchar](20) NULL,
	[fechaExencionExamenProfesional] [nvarchar](20) NULL,
	[cumplioServicioSocial] [nvarchar](10) NULL,
	[idfundamentoLegalServicioSocial] [nvarchar](5) NULL,
	[fundamentoLegalServicioSocial] [nvarchar](max) NULL,
	[idEntidadFederativa] [nvarchar](10) NULL,
	[entidadFederativa] [nvarchar](50) NULL,
	[institucionProcedencia] [nvarchar](max) NULL,
	[idTipoEstudioAntecedente] [nvarchar](50) NULL,
	[tipoEstudioAntecedente] [nvarchar](50) NULL,
	[idEntidadFederativa2] [nvarchar](10) NULL,
	[entidadFederativa2] [nvarchar](50) NULL,
	[fechaInicio2] [nvarchar](20) NULL,
	[fechaTermino2] [nvarchar](20) NULL,
	[noCedula] [nvarchar](20) NULL,
	[STATUS_XML] [nvarchar](10) NULL,
	[AUTENTICADO] [nvarchar](5) NULL,
 CONSTRAINT [PK_REG_TITULOS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[reg_usu]    Script Date: 19/12/2018 12:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reg_usu](
	[id_usu] [int] IDENTITY(1,1) NOT NULL,
	[usu_nombre] [varchar](50) NOT NULL,
	[usu_contrasena] [varchar](50) NOT NULL,
	[usu_correo] [varchar](50) NOT NULL,
	[usu_pregunta] [varchar](50) NOT NULL,
	[usu_respuesta] [varchar](50) NOT NULL,
	[id_rol] [int] NULL,
	[usu_fecmov] [datetime] NOT NULL,
 CONSTRAINT [PK_reg_usu] PRIMARY KEY CLUSTERED 
(
	[id_usu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[REG_Validacion]    Script Date: 19/12/2018 12:13:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[REG_Validacion](
	[id_vplantel] [int] IDENTITY(1,1) NOT NULL,
	[vp_id_unico_cert] [varchar](50) NULL,
	[vp_estatus] [varchar](50) NULL,
	[vp_fec_mov]  AS (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[reg_campus] ON 

INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (1, N'ERMITA')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (2, N'IZAPALAPA')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (3, N'TLALNEPANTLA')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (4, N'XOLA')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (5, N'SUR')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (6, N'CENTRO')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (7, N'CHALCO')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (8, N'CUAUTITLAN')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (9, N'NORTE')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (10, N'TLAHUAC')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (11, N'TLALPAN')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (12, N'VIA MORELOS')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (13, N'VIADUCTO')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (14, N'SAN ANGEL')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (15, N'TOREO')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (16, N'TOLUCA')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (17, N'LEON')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (18, N'ECATEPEC')
INSERT [dbo].[reg_campus] ([id_campus], [campus_nom]) VALUES (19, N'ORIENTE')
SET IDENTITY_INSERT [dbo].[reg_campus] OFF
SET IDENTITY_INSERT [dbo].[reg_usu] ON 

INSERT [dbo].[reg_usu] ([id_usu], [usu_nombre], [usu_contrasena], [usu_correo], [usu_pregunta], [usu_respuesta], [id_rol], [usu_fecmov]) VALUES (4, N'sadot', N'b306dcba2092fe85ff4c5e979d6bdddf3d375638', N'sadot1@gmail.com', N'profe', N'pi', 1, CAST(0x0000A9B90111A696 AS DateTime))
INSERT [dbo].[reg_usu] ([id_usu], [usu_nombre], [usu_contrasena], [usu_correo], [usu_pregunta], [usu_respuesta], [id_rol], [usu_fecmov]) VALUES (6, N'alberto', N'ff0d682466426e09a0127410604aeeba1c4b404d', N'alberto', N'profe', N'pi', 1, CAST(0x0000A9BB00C8473E AS DateTime))
SET IDENTITY_INSERT [dbo].[reg_usu] OFF
SET IDENTITY_INSERT [dbo].[REG_Validacion] ON 

INSERT [dbo].[REG_Validacion] ([id_vplantel], [vp_id_unico_cert], [vp_estatus]) VALUES (37, N'P0000187601118TLDG-018XOLA', N'Gen.XML')
INSERT [dbo].[REG_Validacion] ([id_vplantel], [vp_id_unico_cert], [vp_estatus]) VALUES (38, N'P0000239551118TLDG-029XOLA', N'Gen.XML')
INSERT [dbo].[REG_Validacion] ([id_vplantel], [vp_id_unico_cert], [vp_estatus]) VALUES (39, N'P0000178201118TLDG-008XOLA', N'Gen.XML')
INSERT [dbo].[REG_Validacion] ([id_vplantel], [vp_id_unico_cert], [vp_estatus]) VALUES (40, N'P0000237901118TLDG-027XOLA', N'Gen.XML')
INSERT [dbo].[REG_Validacion] ([id_vplantel], [vp_id_unico_cert], [vp_estatus]) VALUES (41, N'P0000241781118TLDG-003XOLA', N'Gen.XML')
SET IDENTITY_INSERT [dbo].[REG_Validacion] OFF
ALTER TABLE [dbo].[reg_usu] ADD  CONSTRAINT [DF__reg_usu__usu_fec__4EE40818]  DEFAULT (getdate()) FOR [usu_fecmov]
GO
