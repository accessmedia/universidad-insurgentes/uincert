<?php
    header('Content-Type: text/html; charset=UTF-8');
    
    $SecureSection = false;
    require_once("util/utilerias.php");
    require_once("./config/xData.php");
?>

<script src='./js/date-time-picker.min.js'></script>
<script>
    function upfle(pname, countFile){
        var countThisFile   = countFile;
        var uploadFile      = '#Upload'+event.target.id;
        var buttonsFile     = '#Buttons'+event.target.id;
        var loadedFiles     = $("div").find('#LoadedFiles').val();

        var formdata        = new FormData();
        var file            = $(pname)[0].files[0];
        var xtitle          = $(pname).attr("title");
        var idAlumno        = $('#FolioControl').val();
        var ThisCarrer      = $('#CveCarrera').val();
        var pathFile        = $(pname).attr("pathfile");

        formdata.append("file", file);
        formdata.append("idAlumno", idAlumno);
        formdata.append("CveCarrera", ThisCarrer);
        formdata.append("tipoDoc", xtitle);
        formdata.append("pathFile", pathFile);
        formdata.append("FileNumber", countFile);
        formdata.append("thisType", "TitulosPDF");

        jQuery.ajax({
            url: 'upload.php',
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false
            }).done(function(msg){
                if(msg.split(" | ")[0] == 1){
                    $(uploadFile).fadeOut(500, function(){
                        $(buttonsFile).html(msg.split(" | ")[1]);
                        $(buttonsFile).fadeIn(500);
                        if(countThisFile == 1){
                            loadedFiles = parseInt(loadedFiles) + parseInt(1);
                            $("div").find('#LoadedFiles').attr('value', loadedFiles);
                            if(loadedFiles >= 7){
                                $('#ValidateStudent').attr('disabled', false);
                            }else{
                                $('#ValidateStudent').attr('disabled', false);
                            }
                        }
                    });
                }else{
                    $(".main_files").append(msg);
                }
            });
    }

    function mfile(pname){
        var myfiles  = document.getElementById("OtherFiles");
        var files    = myfiles.files;
        var data     = new FormData();
        var pathFile = $(pname).attr("pathfile");

        idAlumno = $('#FolioControl').val();
        data.append("idAlumno", idAlumno);
        data.append("pathFile", pathFile);
        data.append("tipoDoc", "OtherFiles");
        data.append("thisType", "TitulosPDF");
        data.append("CveCarrera", "");
        data.append("FileNumber", "");

        for (i = 0; i < files.length; i++){
            data.append('file' + i, files[i]);
        }

        $.ajax({
            url: 'upload.php', 
            type: 'POST',
            data: data,
            contentType: false,
            processData: false,
            cache: false
            }).done(function(msg){
                $(".other_files").append(msg);
            });
    }

    function deleteThisFile(fileId, countFile){
        if(confirm("Estas seguro que deseas eliminar este archivo?")){
            var countThisFile   = countFile;
            var thisId          = $(fileId).attr('id');
            var thisFile        = $(fileId).attr('file');
            var thisTip         = $(fileId).attr('tip');

            var tipoDoc         = $(fileId).attr("tipoDoc");
            var idAlumno        = $(fileId).attr("fileFolio");
            var ThisCarrer      = $(fileId).attr("fileCarrer");
            
            var data            = $(fileId).serializeArray();
            var loadedFiles     = $("div").find('#LoadedFiles').val();

            data.push({name:'SecType', value:'delete_file'});
            data.push({name:'tipoDoc', value:tipoDoc});
            data.push({name:'idAlumno', value:idAlumno});
            data.push({name:'CveCarrera', value:ThisCarrer});
            data.push({name:'thisFile', value:thisFile});
            
            $.ajax({
                url:        'valplantel.php',
                type:       'post',
                dataType:   'json',
                data: data
            }).always(function(){
                switch(thisTip){
                    case 'newfile': $("div").find('#newfile_'+thisId).fadeOut(500); break;
                    case 'oldfile': $("div").find('#file_'+thisId).fadeOut(500); break;
                    case 'reqfile':
                        $('#Buttonsfile'+thisId).fadeOut(500);
                        $('#Uploadfile'+thisId).delay(500).fadeIn(500);
                    break;
                }
                if(countThisFile == 1){
                    loadedFiles = parseInt(loadedFiles) - parseInt(1);
                    $("div").find('#LoadedFiles').attr('value', loadedFiles);
                    if(loadedFiles < 7){
                        $('#ValidateStudent').attr('disabled', false);
                    }else{
                        $('#ValidateStudent').attr('disabled', false);
                    }
                }
            });
            return false;
        }
    }

    $(document).ready(function(){
        $('.ValidaAlumno').click(function(e){
            e.preventDefault();
            var data        = $(this).serializeArray();
            var VpUnicoCert = $('#VpUnicoCert').val();
            var thisAction  = $(this).attr("status");
            var xSaveData   = true;
            switch(thisAction){
                case "validate":
                    var thisIcon        = "check";
                    var thisClass       = "text-success";
                    var thisStatus      = "Validado";
                    var thisConfirmMsg  = "Esta completamente seguro de querer Validar a este Alumno?";

                    var thisCampus      = $('#Campus').val();
                    var thisStudentId   = $('#StudentId').val();
                    var thisCarrer      = $('#CveCarrera').val();

                    $('#raz_'+VpUnicoCert).html("N/A");

                    if($('#end_date_2').val() < $('#start_date_2').val()){
                        xSaveData       = false;
                        var xMessage    = "La fecha de terminación de antecedente debe ser mayor a la fecha de inicio";
                    };
                    if($('#end_date_2').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que ingrese la Fecha de Terminación de Antecedente";
                    };
                    if($('#idEntidadFederativaExpedicion').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que indique la Entidad Federativa de Expedicion";
                    };
                    if($('#idEntidadFederativaAntecedente').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que indique la Entidad Federativa Antecedente";
                    };
                    if($('#idTipoEstudioAntecedente').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que indique el tipo de Estudio Antecedente";
                    };
                    if($('#institucionProcedencia').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que indique la Institucion de Procedencia";
                    };
                    if($('#idFundamentoLegalServicioSocial').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que indique el Fundamento Legal Servicio Social";
                    };
                    if($('#cumplioServicioSocial').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que indique si cumplio con el Servicio Social";
                    };
                    if($('#idModalidadTitulacion').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que ingrese la Modalidad de Titulacion";
                    };
                    if($('#fechaExpedicion').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que ingrese la Fecha de Expedicion";
                    };
                    if($("#correoElectronico").val().indexOf('@', 0) == -1 || $("#correoElectronico").val().indexOf('.', 0) == -1){
                        xSaveData       = false;
                        var xMessage    = "El Correo Electronico introducido no es correcto. Favor de revisarlo";
                    }
                    if($('#curp').val().length < 18){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que el campo de la CURP tenga valor de 18 caracteres.";
                    };
                    if($('#end_date').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es necesario que ingrese la fecha de terminación de la carrera";
                    };
                break;
                case "rejected":
                    var thisIcon        = "times";
                    var thisClass       = "text-danger";
                    var thisStatus      = "Rechazado";
                    var thisConfirmMsg  = "Esta completamente seguro de querer Rechazar a este Alumno?";

                    $(".tab-pane").removeClass("active");
                    $("#Razon").removeClass("fade");
                    $("#Razon").addClass("active");

                    $('#raz_'+VpUnicoCert).html("<i class='text-warning fa fa-user-times'></i>");
                    if($('#razon_rechazo').val().length == 0){
                        xSaveData       = false;
                        var xMessage    = "Es Necesario que introduzca la razon del rechazo del alumno";
                    }else{
                        xSaveData       = true;
                    }
                break;
            }
            if(xSaveData){
                var opcion = confirm(thisConfirmMsg);
                if(opcion == true){
                    data.push({name:'SecType', value:'TIT'});
                    data.push({name:'StudentId', value:VpUnicoCert});
                    data.push({name:'Status', value:thisStatus});
                    data.push({name:'RazonRechazo', value:$('#razon_rechazo').val()});
                    $.ajax({
                        url: 'valplantel.php',
                        type: 'post',
                        dataType: 'json',
                        data: data
                    })
                    .always(function(){
                        var thisId  = '#img'+$('#FolioControl').val();
                        var name    = '#est'+$('#FolioControl').val();
                        var chkStud = '#CheckAlumno_P'+$('#FolioControl').val();
                        $(name).val(thisStatus);
                        $(thisId).removeClass().addClass(thisClass+' fa fa-'+thisIcon);
                        // $("html, body").animate({scrollTop: 150}, 1500, function(){
                            if(thisAction === "validate"){
       			                $("#GuardaTitulos").fadeOut(800);
       			                $("#ValidateStudent").fadeOut(800);
                                $.post(
                                    "TitulosPlantelList.php",{
                                        thisCampus: thisCampus,
                                        thisCarrer: thisCarrer,
                                        thisStudentId: thisStudentId
                                    },
                                    function(data){
                                        $('#ListAlumnos').html(data);
                                    }
                                );
                            }else{
                                $(chkStud).html('');
       			                $("#RejectStudent").fadeOut(800);
                            }
                        $('#infoModal').modal('hide');
                        // });
                    });
                }
            }else{
                alert(xMessage);
                return false;
            }
        })
    })

    $(document).ready(function(){
        $("#fechaExamenProfesional").click(function(){
            $('#fechaExencionExamenProfesional').val('');
        });
        $("#fechaExencionExamenProfesional").click(function(){
            $('#fechaExamenProfesional').val('');
        });
    })

    $(document).ready(function(){
        if($("#idTipoEstudioAntecedente").val() <= 2){
            $("#noCedula").prop('disabled', false);
        }else{
            $("#noCedula").val('');
            $("#noCedula").prop('disabled', true);
        }
        $("#idTipoEstudioAntecedente").click(function(){
            if($("#idTipoEstudioAntecedente").val() <= 2){
                $("#noCedula").prop('disabled', false);
            }else{
                $("#noCedula").val('');
                $("#noCedula").prop('disabled', true);
            }
        });
    })

    $(document).ready(function(){
        $('#ValidaDatosTitulo').submit(function(e){
            e.preventDefault();
            var xSaveData = true;
            
            if($('#end_date_2').val() < $('#start_date_2').val()){
                xSaveData       = false;
                var xMessage    = "La fecha de terminación de antecedente debe ser mayor a la fecha de inicio";
            };
            if($('#end_date_2').val().length == 0){
                xSaveData       = false;
                var xMessage    = "Es necesario que ingrese la Fecha de Terminación de Antecedente";
            };
            if($('#idEntidadFederativaExpedicion').val().length == 0){
                xSaveData       = false;
                var xMessage    = "Es necesario que indique la Entidad Federativa de Expedicion";
            };
            if($('#idEntidadFederativaAntecedente').val().length == 0){
                xSaveData       = false;
                var xMessage    = "Es necesario que indique la Entidad Federativa Antecedente";
            };
            if($('#idTipoEstudioAntecedente').val().length == 0){
                xSaveData       = false;
                var xMessage    = "Es necesario que indique el tipo de Estudio Antecedente";
            };
            if($('#institucionProcedencia').val().length == 0){
                xSaveData       = false;
                var xMessage    = "Es necesario que indique la Institucion de Procedencia";
            };
            if($('#idFundamentoLegalServicioSocial').val().length == 0){
                xSaveData       = false;
                var xMessage    = "Es necesario que indique el Fundamento Legal Servicio Social";
            };
            if($('#cumplioServicioSocial').val().length == 0){
                xSaveData       = false;
                var xMessage    = "Es necesario que indique si cumplió con el Servicio Social";
            };
            if($('#idModalidadTitulacion').val().length == 0){
                xSaveData       = false;
                var xMessage    = "Es necesario que ingrese la Modalidad de Titulación";
            };
            if($('#fechaExpedicion').val().length == 0){
                xSaveData       = false;
                var xMessage    = "Es necesario que ingrese la Fecha de Expedición";
            };
            if($("#correoElectronico").val().indexOf('@', 0) == -1 || $("#correoElectronico").val().indexOf('.', 0) == -1){
                xSaveData       = false;
                var xMessage    = "El Correo Electrónico introducido no es correcto. Favor de revisarlo";
            }
            if($('#curp').val().length < 18){
                xSaveData       = false;
                var xMessage    = "Es necesario que el campo de la CURP tenga valor de 18 caracteres.";
            };
            if($('#end_date').val().length == 0){
                xSaveData       = false;
                var xMessage    = "Es necesario que ingrese la fecha de terminación de la carrera";
            };
            var xSaveData = true;
            
            if(xSaveData){
                var data = $(this).serializeArray();
                data.push({name:'SecType', value:'GuardaDatosTitulo'});
                data.push({name:'EntidadFederativaExpedicion', value:$('#idEntidadFederativaExpedicion option:selected').text()});
                data.push({name:'EntidadFederativaAntecedente', value:$('#idEntidadFederativaAntecedente option:selected').text()});
                data.push({name:'TipoEstudioAntecedente', value:$('#idTipoEstudioAntecedente option:selected').text()});
                data.push({name:'ModalidadTitulacion', value:$('#idModalidadTitulacion option:selected').text()});
                data.push({name:'FundamentoLegalServicioSocial', value:$('#idFundamentoLegalServicioSocial option:selected').text()});
                $.ajax({
                    url: 'valplantel.php',
                    type: 'post',
                    dataType: 'json',
                    data: data
                })
                .always(function(){
                    $("#ValidaDatosTitulo").fadeOut(600);
                    $("#DatosGuardados").fadeIn(600).delay(1000).fadeOut(600, function(){
                        $("#ValidaDatosTitulo").fadeIn(600);
                    })
                });
            }else{
                alert(xMessage);
                return false;
            }
        })
    })
</script>

<?php
    $cont = 0;
    $obj = new Utilerias;
    $obj->CnnBD();

    $CVECAMPUS      = $_GET['campusId'];
    $CVECARRERA     = $_GET['carrerId'];
    $vNumControl    = $_GET['vNumControl'];
  
    $query  = str_replace("VARIABLE=CAMPUS", $CVECAMPUS, TITULOS);
    $query  = str_replace("VARIABLE=CARRERA", $CVECARRERA, $query);      
    $query  = str_replace("VARIABLE=PEOPLE_CODE_ID", $vNumControl, $query);
    $rQuery = $obj->xQuery($query);

    $ValidateGranted = true;
    if($data = sqlsrv_fetch_array($rQuery)){
        $getDbName      = "Info_Validacion";
        $FolioCont      = utf8_encode($data["folioControl"]);
        $getDbFilterCv  = "WHERE folioControl = '".$FolioCont."' AND CveCarrera = '".utf8_encode($data["cveCarrera"])."'";
        $thisStartDate  = $obj->getDbRowName("fechaInicio", $getDbName, $getDbFilterCv, 0);
        $thisEndDate    = $obj->getDbRowName("fechaTerminacion", $getDbName, $getDbFilterCv, 0);
        $thisExpDate    = $obj->getDbRowName("fechaExpedicion", $getDbName, $getDbFilterCv, 0);
        $thisProfDate   = $obj->getDbRowName("fechaExamenProfesional", $getDbName, $getDbFilterCv, 0);
        $thisExProfDate = $obj->getDbRowName("fechaExencionExamenProfesional", $getDbName, $getDbFilterCv, 0);
        $thisStartDate2 = $obj->getDbRowName("fechaInicio2", $getDbName, $getDbFilterCv, 0);
        $thisEndDate2   = $obj->getDbRowName("fechaTerminacion2", $getDbName, $getDbFilterCv, 0);

        if($thisStartDate <> ""){$thisStartDate = $thisStartDate->format('Y-m-d');}
        if($thisEndDate <> ""){$thisEndDate = $thisEndDate->format('Y-m-d');}
        if($thisExpDate <> ""){$thisExpDate = $thisExpDate->format('Y-m-d');}
        if($thisProfDate <> ""){$thisProfDate = $thisProfDate->format('Y-m-d');}
        if($thisExProfDate <> ""){$thisExProfDate = $thisExProfDate->format('Y-m-d');}
        if($thisStartDate2 <> ""){$thisStartDate2 = $thisStartDate2->format('Y-m-d');}
        if($thisEndDate2 <> ""){$thisEndDate2 = $thisEndDate2->format('Y-m-d');}

        if($thisStartDate === "1900-01-01"){$thisStartDate = "";}
        if($thisEndDate === "1900-01-01"){$thisEndDate = "";}
        if($thisExpDate === "1900-01-01"){$thisExpDate = "";}
        if($thisProfDate === "1900-01-01"){$thisProfDate = "";}
        if($thisExProfDate === "1900-01-01"){$thisExProfDate = "";}
        if($thisStartDate2 === "1900-01-01"){$thisStartDate2 = "";}
        if($thisEndDate2 === "1900-01-01"){$thisEndDate2 = "";}

        $ValidationType = $obj->getDbRowName("vp_estatus", "Reg_Validacion", "WHERE vp_id_unico_cert = 'P".utf8_encode($data["folioControl"])."_".utf8_encode($data["cveCarrera"])."'", 0);
        $DeniedReason   = $obj->getDbRowName("vp_razon_rechazo", "Reg_Validacion", "WHERE vp_id_unico_cert = 'P".utf8_encode($data["folioControl"])."_".utf8_encode($data["cveCarrera"])."'", 0);
        $ValidateStatus = $obj->getDbRowName("vp_estatus", "Reg_Validacion", "WHERE vp_id_unico_cert = 'P".utf8_encode($data["folioControl"])."_".utf8_encode($data["cveCarrera"])."'", 0);

        /*echo "<br><h5> Titulación </h5><br>";*/
        echo "<h5>".utf8_encode($data["folioControl"])."  ".utf8_encode($data["nombre"])." ".utf8_encode($data["primerApellido"])." ".utf8_encode($data["segundoApellido"])." <span class='label label-default'></span></h5>";
        echo "
              <ul id='TabFormulario' class='nav nav-tabs' role='tablist'>
                <li class='nav-item'><a class='nav-link stdMenu active' data-toggle='tab' href='#TituloElectronico'>Título Electrónico</a></li>
                <li class='nav-item'><a class='nav-link stdMenu' data-toggle='tab' href='#Institucion'>Institución</a></li>
                <li class='nav-item'><a class='nav-link stdMenu' data-toggle='tab' href='#Carrera'>Carrera</a></li>
                <li class='nav-item'><a class='nav-link stdMenu' data-toggle='tab' href='#Profesionista'>Profesionista</a></li>
                <li class='nav-item'><a class='nav-link stdMenu' data-toggle='tab' href='#Expedicion'>Expedici&oacute;n</a></li>
                <li class='nav-item'><a class='nav-link stdMenu' data-toggle='tab' href='#Antecedente'>Antecedente</a></li>
                <li class='nav-item'><a class='nav-link stdMenu' data-toggle='tab' href='#Documentos'>Documentos</a></li>
                <li class='nav-item'><a class='nav-link' data-toggle='tab' href='#Razon'>Raz&oacute;n Rechazo</a></li>
              </ul>
              ";
        echo "
            <div class='tab-content' id='DatosGuardados' style='display:none;'>
                <div id='CheckData' style='width:100%; height:400px; text-align:center;'><i class='fa fa-check-circle' aria-hidden='true' style='font-size:200px; color:cadetblue;'></i><br /><span style='size:30px; color:cadetblue;'>Datos Guardados</span></div>
            </div>
            <form id='ValidaDatosTitulo' method='post' enctype='multipart/form-data'>
                <div class='tab-content' id='FormularioTitulos'>
                    <div id='TituloElectronico' class='container tab-pane active'><br>
                        <div class='row'>
                            <div class='col-sm-3'><p class='navbar-text'>Folio Control</p></div>
                            <div class='col-sm-6'>
                                <input id='DisplayFolioControl' name='DisplayFolioControl' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["folioControl"])."' disabled/>
                                <input type='hidden' id='FolioControl' name='FolioControl' value='".$FolioCont."' />
                                <input type='hidden' id='IdAlumno' name='IdAlumno' value='".utf8_encode($data["folioControl"])."' />
                                <input type='hidden' id='VpUnicoCert' name='VpUnicoCert' value='P".utf8_encode($data["folioControl"])."_".utf8_encode($data["cveCarrera"])."' />
                            </div> 
                        </div>
                    </div>
                    <div id='Institucion' class='container tab-pane'><br>
                        <div class='row'>
                            <div class='col-sm-1'><p class='navbar-text'>Institución </p></div>
                            <div class='col-sm-10'>
                                <input id='cveInstitucion' name='cveInstitucion' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["nombreInstitucion"])."' disabled/>
                            </div> 
                        </div>
                    </div>
                    <div id='Carrera' class='container tab-pane'><br>
                        <div class='row'>
                            <div class='col-sm-3'><p class='navbar-text'>Carrera</p></div>
                            <div class='col-sm-6'>
                                <input type='hidden' id='CveCarrera' name='CveCarrera' value='".utf8_encode($data["cveCarrera"])."' />
                                <input id='DisplayCveCarrera' name='DisplayCveCarrera' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["cveCarrera"])."' disabled/><br />
                                <input id='nombreCarrera' name='nombreCarrera' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["nombreCarrera"])."' disabled/><br />
                            </div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><p class='navbar-text'>F. Inicio</p></div>
                            <div class='col-sm-3'>
                                <input id='start_date' name='start_date' class='form-control' value='".$thisStartDate."'>
                           </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>F. Terminaci&oacute;n</p></div>
                            <div class='col-sm-3'>
                                <input id='end_date' name='end_date' class='form-control' value='".$thisEndDate."'>
                           </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><p class='navbar-text'>Reconocimiento</p></div>
                            <div class='col-sm-6'>
                                <input type='hidden' id='idAutorizacionReconocimiento' name='idAutorizacionReconocimiento' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["idAutorizacionReconocimiento"])."' />
                                <input id='AutorizacionReconocimiento' name='AutorizacionReconocimiento' type='text' class='form-control' autocomplete='false' value='".utf8_encode("RVOE FEDERAL")."' disabled/>
                            </div> 
                        </div>                
                        <div class='row'>
                            <div class='col-sm-3'><p class='navbar-text'>RVOE</p></div>
                            <div class='col-sm-6'>
                                <input id='numeroRvoe' name='numeroRvoe' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["numeroRvoe"])."' disabled/>
                            </div> 
                        </div>
                        <script type='text/javascript'>
                            $('#start_date').css('background-color', '#ffffff');
                            $('#end_date').css('background-color', '#ffffff');

                            $('#start_date').dateTimePicker({
                              limitMax: $.now()
                            });
                            $('#end_date').dateTimePicker({
                              limitMin: $('#start_date'),
                              limitMax: $.now()
                            });
                        </script>
                    </div>
                    <div id='Profesionista' class='container tab-pane'><br>
                        <div class='row'>
                            <div class='col-sm-3'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>CURP</p></div>
                            <div class='col-sm-6'>
                                <input id='curp' name='curp' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["curp"])."' maxlength='18'/>
                            </div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><p class='navbar-text'>Nombre</p></div>
                            <div class='col-sm-6'>
                                <input id='nombre' name='nombre' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["nombre"])."' disabled/>
                            </div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><p class='navbar-text'>A. Paterno</p></div>
                            <div class='col-sm-6'>
                                <input id='primerApellido' name='primerApellido' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["primerApellido"])."' disabled/>
                            </div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><p class='navbar-text'>A. Materno</p></div>
                            <div class='col-sm-6'>
                                <input id='segundoApellido' name='segundoApellido' type='text' class='form-control' autocomplete='false' value='".utf8_encode($data["segundoApellido"])."' disabled/>
                            </div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>Correo electrónico</p></div>
                            <div class='col-sm-6'>
                                <input type='email' id='correoElectronico' name='correoElectronico' class='form-control' autocomplete='false' value='".$obj->getDbRowName("correoElectronico", $getDbName, $getDbFilterCv, 0)."'/>
                            </div> 
                        </div>
                    </div>
                    <div id='Expedicion' class='container tab-pane'><br>
                        <div class='row'>
                            <div class='col-sm-4'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>F. Expedición </p></div>
                            <div class='col-sm-3'>
                                <input id='fechaExpedicion' name='fechaExpedicion' class='form-control' value='".$thisExpDate."'>
                           </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-4'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>Mod. Titulación</p></div>
                            <div class='col-sm-6'>".$obj->cat_ModTitulacion('idModalidadTitulacion', $obj->getDbRowName("idModalidadTitulacion", $getDbName, $getDbFilterCv, 0))."</div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-4'><p class='navbar-text'>Fecha de Examen Profesional:<br /><small>&Uacute;nicamente podr&aacute; elegir una fecha</small></p></div>
                            <div class='col-sm-8'>
                                <div class='row'>
                                    <div class='col-sm-4'>
                                        Examen Profesional:<br />
                                        <input id='fechaExamenProfesional' name='fechaExamenProfesional' class='form-control' value='".$thisProfDate."'>
                                    </div>
                                    <span style='margin-top:30px;'>Ó</span>
                                    <div class='col-sm-3'>
                                        Exención Examen:<br />
                                        <input id='fechaExencionExamenProfesional' name='fechaExencionExamenProfesional' class='form-control' value='".$thisExProfDate."'>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-4'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>Servicio Social</p></div>
                            <div class='col-sm-6'>".$obj->cat_ServSocial('cumplioServicioSocial', $obj->getDbRowName("cumplioServicioSocial", $getDbName, $getDbFilterCv, 0))."</div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-4'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>Fundamento Legal Servicio Social</p></div>
                            <div class='col-sm-6'>".$obj->cat_FundLegal('idFundamentoLegalServicioSocial', $obj->getDbRowName("idFundamentoLegalServicioSocial", $getDbName, $getDbFilterCv, 0))."</div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-4'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>Entidad Federativa de Expedición</p></div>
                            <div class='col-sm-6'>".$obj->cat_EntFed('idEntidadFederativaExpedicion', $obj->getDbRowName("idEntidadFederativaExpedicion", $getDbName, $getDbFilterCv, 0))."</div> 
                        </div>
                        <script type='text/javascript'>
                            $('#fechaExpedicion').css('background-color', '#ffffff');
                            $('#fechaExamenProfesional').css('background-color', '#ffffff');
                            $('#fechaExencionExamenProfesional').css('background-color', '#ffffff');

                            $('#fechaExpedicion').dateTimePicker({
                              limitMax: $.now()
                            });
                            $('#fechaExamenProfesional').dateTimePicker({
                              limitMax: $.now()
                            });
                            $('#fechaExencionExamenProfesional').dateTimePicker({
                              limitMax: $.now()
                            });
                        </script>
                    </div>
                    <div id='Antecedente' class='container tab-pane'><br>
                        <div class='row'>
                            <div class='col-sm-3'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>Institución procedencia</p></div>
                            <div class='col-sm-6'>
                                <input id='institucionProcedencia' name='institucionProcedencia' type='text' class='form-control' autocomplete='false' value='".$obj->getDbRowName("institucionProcedencia", $getDbName, $getDbFilterCv, 0)."' />
                            </div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>Tipo Estudio Antecedente</p></div>
                            <div class='col-sm-6'>".$obj->cat_TipoEstudios('idTipoEstudioAntecedente', $obj->getDbRowName("idTipoEstudioAntecedente", $getDbName, $getDbFilterCv, 0))."</div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>Entidad Federativa</p></div>
                            <div class='col-sm-6'>".$obj->cat_Estados('idEntidadFederativaAntecedente', $obj->getDbRowName("idEntidadFederativaAntecedente", $getDbName, $getDbFilterCv, 0))."</div> 
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><p class='navbar-text'>F. Inicio</p></div>
                            <div class='col-sm-3'>
                                <input id='start_date_2' name='start_date_2' class='form-control' value='".$thisStartDate2."' required>
                           </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>F. Terminaci&oacute;n</p></div>
                            <div class='col-sm-3'>
                                <input id='end_date_2' name='end_date_2' class='form-control' value='".$thisEndDate2."' required>
                           </div>
                        </div>
                        <div class='row'>
                            <div class='col-sm-3'><p class='navbar-text'>No Cédula<br /><small>Aplicable solo para Licenciatura y Maestría</small></p></div>
                            <div class='col-sm-6'>
                                <input id='noCedula' name='noCedula' type='text' class='form-control' autocomplete='false' value='".$obj->getDbRowName("noCedula", $getDbName, $getDbFilterCv, 0)."' />
                            </div> 
                        </div>
                        <script type='text/javascript'>
                            $('#start_date_2').css('background-color', '#ffffff');
                            $('#end_date_2').css('background-color', '#ffffff');

                            $('#start_date_2').dateTimePicker({
                              limitMax: $.now()
                            });
                            $('#end_date_2').dateTimePicker({
                              limitMin: $('#start_date_2'),
                              limitMax: $.now()
                            });
                        </script>
                    </div>

                    <div id='Documentos' class='container tab-pane fade'><br>
                        <table id='dtDynamicVerticalScrollExample' class='main_files table table-rounded table-striped table-sm dataTable'  cellspacing='0' width='100%' style='text-align: center; margin-left: 0px;' role='grid'>
                          <thead>
                                <tr class='bg-primary rounded-top text-white' style='text-align:center; background-color:#67a1fc;' role='row'>
                                    <th>Tipo de Documento</th>
                                    <th>Acci&oacute;n</th>
                                </tr>
                            </thead>
                            <tbody>";
                                $LoadedFiles = count(glob('./TitulosPDF/'.trim('P'.$vNumControl).'/{*.pdf}',GLOB_BRACE));
                                echo "<input type='hidden' id='LoadedFiles' value='".$LoadedFiles."'>";
                                for($i=1; $i<=8; $i++){
                                    $CountthisFile[1]   = 1;
                                    $GrantedFile[1]     = false;
                                    $IsRequired[1]      = true;
                                    $thisFileName[1]    = "Acta de Nacimiento";
                                    $thisDivName[1]     = "ActaDeNacimientoTitulo";

                                    $CountthisFile[2]   = 1;
                                    $GrantedFile[2]     = false;
                                    $IsRequired[2]      = true;
                                    $thisFileName[2]    = "CURP 200%";
                                    $thisDivName[2]     = "CURPTitulo";

                                    $CountthisFile[3]   = 1;
                                    $GrantedFile[3]     = false;
                                    $IsRequired[3]      = true;
                                    $thisFileName[3]    = "Certificado de Estudio";
                                    $thisDivName[3]     = "CertificadoDeEstudioTitulo";

                                    $CountthisFile[4]   = 1;
                                    $GrantedFile[4]     = false;
                                    $IsRequired[4]      = true;
                                    $thisFileName[4]    = "Acta de presentación de Examen Profesional";
                                    $thisDivName[4]     = "ActaDePresentacionDeExamenProfesional";

                                    $CountthisFile[5]   = 0;
                                    $GrantedFile[5]     = true;
                                    $IsRequired[5]      = false;
                                    $thisFileName[5]    = "Equivalencia o revalidación de estudios";
                                    $thisDivName[5]     = "EquivalenciaORevalidacion";

                                    $CountthisFile[6]   = 1;
                                    $GrantedFile[6]     = false;
                                    $IsRequired[6]      = true;
                                    $thisFileName[6]    = "Liberación de servicio social";
                                    $thisDivName[6]     = "LiberacionDeServicioSocial";

                                    $CountthisFile[7]   = 1;
                                    $GrantedFile[7]     = false;
                                    $IsRequired[7]      = true;
                                    $thisFileName[7]    = "Pago de trámite de titulación";
                                    $thisDivName[7]     = "PagoDeTramite";

                                    $CountthisFile[8]   = 1;
                                    $GrantedFile[8]     = false;
                                    $IsRequired[8]      = true;
                                    $thisFileName[8]    = "Constancia de no adeudo";
                                    $thisDivName[8]     = "ContanciaDeNoAdeudo";

                                    echo "<script>";
                                    echo "    $('#upfile".$i."').click(function(){";
                                    echo "        $('#file".$i."').trigger('click');";
                                    echo "    });";
                                    echo "</script>";

                                    $DisplayButtons[$i] = "none";
                                    $thisPath           = "./TitulosPDF/".trim('P'.$vNumControl);
                                    
                                    if(!file_exists($thisPath)){mkdir($thisPath, 0777, true);}
                                    if(file_exists($thisPath)){
                                        $directorio = opendir($thisPath);
                                        $ThisFile = $obj->getDbRowName("NombreDocumento", "Info_Documentos", "WHERE FolioControl = '".$FolioCont."' AND CveCarrera = '".utf8_encode($data["cveCarrera"])."' AND TipoDocumento = '".$thisDivName[$i]."'", 1);
                                        if($ThisFile === "error"){
                                            $ThisFile = $thisDivName[$i]."_P".$vNumControl.".pdf";
                                            while($archivo = readdir($directorio)){                   
                                                if(!is_dir($archivo)){
                                                    $pos = strpos($archivo, '_');
                                                    $fcad = substr($archivo, 0, $pos);
                                                    if($fcad === $thisDivName[$i]){
                                                        $DisplayButtons[$i] = "inline";
                                                        $GrantedFile[$i]    = true;
                                                    }
                                                }
                                            }
                                        }else{
                                            $DisplayButtons[$i] = "inline";
                                            $GrantedFile[$i]    = true;
                                        }
                                    }
                                    
                                    if($GrantedFile[$i] == false){$ValidateGranted = false;}
                                    if($DisplayButtons[$i] === "inline"){$DisplayUpload[$i] = "none";}else{$DisplayUpload[$i] = "inline";}

                                    if($ValidateGranted){
                                        echo "<script>";
                                        echo "  $('#ValidateStudent').attr('disabled', false);";
                                        echo "</script>";
                                    }else{
                                        echo "<script>";
                                        echo "  $('#ValidateStudent').attr('disabled', false);";
                                        echo "</script>"; 
                                    }

                                    echo "<tr>";
                                    if($IsRequired[$i]){
                                        echo "  <td id='FileType' style='text-align:left;'><span style='color:#ff0000; float:left; margin-top:-5px;'><h3>*</h3></span> <b>".$thisFileName[$i]."</b></td>";
                                    }else{
                                        echo "  <td id='FileType' style='text-align:left;'><b>".$thisFileName[$i]."</b></td>";
                                    }
                                    echo "  <td style='text-align:center;'>";
                                    echo "      <input type='file' id='file".$i."' name='file".$i."' style='display:none' title='".$thisDivName[$i]."' pathfile='".$thisPath."' onchange='upfle(this, ".$i.")' accept='application/pdf'/>";
                                    echo "      <div id='Uploadfile".$i."' class='UploadNewFile' style='display:".$DisplayUpload[$i].";'><span id='upfile".$i."' class='btn btn-success' style='padding:5px;'><i class='fa fa-upload'></i> Cargar</span></div>";
                                    echo "      <div id='Buttonsfile".$i."' style='display:".$DisplayButtons[$i].";'>";
                                    echo "          <a onclick=window.open('".$thisPath."/".$ThisFile."');><span id='viewfile".$i."' class='btn btn-info' style='padding:5px;'><i class='fa fa-eye'></i> Ver Archivo</span></a><a id='".$i."' file='".$thisPath."/".$ThisFile."' tip='reqfile' tipoDoc='".$thisDivName[$i]."' fileFolio='".$FolioCont."' fileCarrer='".utf8_encode($data["cveCarrera"])."' onclick='deleteThisFile(this, ".$CountthisFile[$i].")'><span id='delete".$i."' class='btn btn-danger UploadNewFile' style='padding:5px;'><i class='fa fa-times'></i> Eliminar</span></a>";
                                    echo "      </div>";
                                    echo "  </td>";
                                    echo "</tr>";
                                }
                            echo "
                            </tbody>
                        </table>
                        
                        <div class='row'>
                            <div class='col-sm-12'>
                                <div class='dataTables_scrollHead' style='overflow: hidden; position: relative; border: 0px; width: 100%;'> 
                                    <div class='dataTables_scrollHeadInner' style='box-sizing: content-box;/* width: 1301px;*/ padding-right: 0px;'>
                                        <h4 style='margin-top:20px;'>Otros Documentos</h4>
                                        <table id='dtDynamicVerticalScrollExample' class='other_files table table-rounded table-striped table-sm dataTable'  cellspacing='0' width='100%' style='text-align: center; margin-left: 0px;' role='grid'>
                                            <thead>
                                                <tr class='bg-primary rounded-top text-white' style='text-align:center; background-color:#67a1fc;' role='row'>
                                                    <th>Tipo de Documento</th>
                                                    <th>Acci&oacute;n</th>
                                                </tr>
                                            </thead>
                                            <tbody>";
                                                $OthersFilesPath = "./TitulosPDF/".trim('P'.$vNumControl)."/otros/";
                                                if(!file_exists($OthersFilesPath)){mkdir($OthersFilesPath, 0777, true);}
                                                echo "<script>";
                                                echo "    $('#UploadOthers').click(function(){";
                                                echo "        $('#OtherFiles').trigger('click');";
                                                echo "    });";
                                                echo "</script>"; 
                                                echo "<tr>";
                                                echo "  <td id='FileType' style='text-align:center;'><b>Otros Documentos</b></td>";
                                                echo "  <td style='text-align:center;'>";
                                                echo "      <input id='OtherFiles' type='file' name='OtherFiles[]' multiple='multiple' pathfile='".$OthersFilesPath."' onchange='mfile(this)' style='display:none' accept='application/pdf'/>";
                                                echo "      <div id='UploadOthers' class='UploadNewFile' style=''><span id='upOtherFile' class='btn btn-success' style='padding:5px;'><i class='fa fa-upload'></i> Cargar Archivos</span></div>";
                                                echo "  </td>";
                                                echo "</tr>";
                                                if(file_exists($OthersFilesPath)){
                                                    $TotalFiles = 0;
                                                    $directorio = opendir($OthersFilesPath);
                                                    while($archivo = readdir($directorio)){
                                                        if(!is_dir($archivo)){
                                                            $TotalFiles = $TotalFiles + 1;
                                                            echo "<tr id='file_".$TotalFiles."'><td id='FileType' style='text-align:center;'>Archivo Cargado</td><td style='text-align:center;'><div id='ButtonsFile".$archivo."' style='text-align:center;'><a onclick=window.open('".$OthersFilesPath."/".$archivo."');><span id='ViewFile".$archivo."' class='btn btn-info' style='padding:5px;'><i class='fa fa-eye'></i> Ver Archivo</span></a><a id='".$TotalFiles."' file='".$OthersFilesPath."/".$archivo."' tip='oldfile' onclick='deleteThisFile(this, 0);'><span id='deleteFile' class='btn btn-danger UploadNewFile' style='padding:5px;'><i class='fa fa-times'></i> Eliminar</span></a></div></td></tr>";
                                                        }
                                                    }
                                                }
                                            echo "
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id='Razon' class='container tab-pane fade'><br>
                        <div class='row'>               
                            <div class='col-sm-3'><span style='color:#ff0000; float:left; margin-top:5px;'><h2>*</h2></span><p class='navbar-text'>Raz&oacute;n de Rechazo</p></div>
                            <div class='col-sm-6'>
                                <textarea name='razon_rechazo' id='razon_rechazo' rows='5' cols='45' class='form-control'>".$DeniedReason."</textarea>
                            </div>
                        </div>
                    </div>
                    <b>Notas:</b>
                    <ol>
                        <li>Todos los campos marcados con: <span class='text-danger' style='size:15px;'><b>*</b></span> son requeridos</li>
                        <li>Para <span class='text-info'><b>GUARDAR</b></span> un alumno compruebe que contiene toda la informaci&oacute;n correcta</li>
                        <li>Para <span class='text-success'><b>VALIDAR</b></span> un alumno es necesario llenar todos los campos requeridos y cargar todos los documentos necesarios</li>
                        <li>Para <span class='text-danger'><b>RECHAZAR</b></span> un alumno es necesario ingresar la raz&oacute;n por la cual ha sido rechazado</li>
                    </ol>";

                    $ShowCalButton = true;
                    if($thisEndDate == ""){$ShowCalButton = false;}
                    if($data["curp"] == ""){$ShowCalButton = false;}
                    if($obj->getDbRowName("correoElectronico", $getDbName, $getDbFilterCv, 0) == ""){$ShowCalButton = false;}
                    if($thisExpDate == ""){$ShowCalButton = false;}
                    if($obj->getDbRowName("idModalidadTitulacion", $getDbName, $getDbFilterCv, 0) == ""){$ShowCalButton = false;}
                    if($obj->getDbRowName("cumplioServicioSocial", $getDbName, $getDbFilterCv, 0) == ""){$ShowCalButton = false;}
                    if($obj->getDbRowName("idFundamentoLegalServicioSocial", $getDbName, $getDbFilterCv, 0) == ""){$ShowCalButton = false;}
                    if($obj->getDbRowName("institucionProcedencia", $getDbName, $getDbFilterCv, 0) == ""){$ShowCalButton = false;}
                    if($obj->getDbRowName("idTipoEstudioAntecedente", $getDbName, $getDbFilterCv, 0) == ""){$ShowCalButton = false;}
                    if($obj->getDbRowName("idEntidadFederativaExpedicion", $getDbName, $getDbFilterCv, 0) == ""){$ShowCalButton = false;}
                    if($obj->getDbRowName("idEntidadFederativaAntecedente", $getDbName, $getDbFilterCv, 0) == ""){$ShowCalButton = false;}
                    if($thisStartDate2 == ""){$ShowCalButton = false;}
                    if($thisEndDate2 == ""){$ShowCalButton = false;}

                    echo "
                    <div style='width:100%; text-align:center;'>
                        ";
                        switch($_SESSION["rol"]){
                            case 4:
                                switch($ValidateStatus){
                                    case '':
                                    case 'Prospecto':
                                        echo "<button type='submit' id='GuardaTitulos' class='btn btn-primary' style='padding:5px;'>";
                                        echo "    <i class='fa fa-save'></i>";
                                        echo "    <span>Guardar</span>";
                                        echo "</button>";
                                        echo "<button type='button' id='ValidateStudent' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
                                        echo "    <i class='fa fa-check'></i>";
                                        echo "    <span>Validar</span>";
                                        echo "</button>";
                                        if($ValidateStatus <> "Validado" OR $ValidateStatus <> "Rechazado"){
                                            echo "<button type='button' id='RejectStudent' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
                                            echo "    <i class='fa fa-check'></i>";
                                            echo "    <span>Rechazar</span>";
                                            echo "</button>";
                                        }
                                    break;
                                    case 'Gen.XML':
                                    case 'XML timbrado':
                                    case 'XML autenticado':
                                    case 'Validado por CSE':
                                        echo "<script>";
                                        echo "  $(document).ready(function(){";
                                        echo "      $('.UploadNewFile').hide();";
                                        echo "  })";
                                        echo "</script>";
                                    break;
                                    case 'Validado':
                                        echo "<button type='button' id='RejectStudent' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
                                        echo "    <i class='fa fa-check'></i>";
                                        echo "    <span>Rechazar</span>";
                                        echo "</button>";
                                    break;
                                    case 'Rechazado':
                                    case 'Rechazado por CSE':
                                    case 'Rechazado por plantel':
                                        echo "<button type='submit' id='GuardaTitulos' class='btn btn-primary' style='padding:5px;'>";
                                        echo "    <i class='fa fa-save'></i>";
                                        echo "    <span>Guardar</span>";
                                        echo "</button>";
                                        echo "<button type='button' id='ValidateStudent' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
                                        echo "    <i class='fa fa-check'></i>";
                                        echo "    <span>Validar</span>";
                                        echo "</button>";
                                    break;
                                }
                            break;
                            default:
                                switch($ValidateStatus){
                                    case '':
                                    case 'Prospecto':
                                        echo "<button type='submit' id='GuardaTitulos' class='btn btn-primary' style='padding:5px;'>";
                                        echo "    <i class='fa fa-save'></i>";
                                        echo "    <span>Guardar</span>";
                                        echo "</button>";
                                        echo "<button type='button' id='ValidateStudent' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
                                        echo "    <i class='fa fa-check'></i>";
                                        echo "    <span>Validar</span>";
                                        echo "</button>";
                                        echo "<button type='button' id='RejectStudent' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
                                        echo "    <i class='fa fa-check'></i>";
                                        echo "    <span>Rechazar</span>";
                                        echo "</button>";
                                    break;
                                    case 'Gen.XML':
                                    case 'Validado':
                                    case 'XML timbrado':
                                        echo "<button type='button' id='RejectStudent' class='btn btn-danger ValidaAlumno' status='rejected' style='padding:5px;'>";
                                        echo "    <i class='fa fa-check'></i>";
                                        echo "    <span>Rechazar</span>";
                                        echo "</button>";
                                    break;
                                    case 'XML autenticado':
                                        echo "<script>";
                                        echo "  $(document).ready(function(){";
                                        echo "      $('.UploadNewFile').hide();";
                                        echo "  })";
                                        echo "</script>";
                                    break;
                                    case 'Rechazado':
                                    case 'Rechazado por CSE':
                                    case 'Rechazado por plantel':
                                        echo "<button type='submit' id='GuardaTitulos' class='btn btn-primary' style='padding:5px;'>";
                                        echo "    <i class='fa fa-save'></i>";
                                        echo "    <span>Guardar</span>";
                                        echo "</button>";
                                        echo "<button type='button' id='ValidateStudent' class='btn btn-success ValidaAlumno' status='validate' style='padding:5px;'>";
                                        echo "    <i class='fa fa-check'></i>";
                                        echo "    <span>Validar</span>";
                                        echo "</button>";
                                    break;
                                }
                            break;
                        }
                        echo "
                    </div>
                </div>
            </form>
        ";
    }
?>