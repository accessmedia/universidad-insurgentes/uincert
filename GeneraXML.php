<?php
    header('Content-Type: text/html; charset=UTF-8');
?>

<script src='./js/date-time-picker.min.js'></script>
<?php $thisType = $_GET['SecType'];?>

<script> 
    function getZipFile(pname){
        objname   = '#up'+event.target.id;
        formdata  = new FormData();
        file      = $(pname)[0].files[0];
        xtitle    = $(pname).attr("title");

        formdata.append("file", file);
        formdata.append("tipoDoc", xtitle);
        <?php echo "formdata.append('SecType', '".$thisType."');"; ?>

        jQuery.ajax({
            url: "GuardaDatosXML.php",
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false
        }).done(function(msg){
            switch(msg.trim()){
                case "0":
                    alert("Se ha detectado un error dentro del archivo .zip cargado, favor de revisarlo");
                break;
                default:
                    $("#thisFile").fadeOut(600);
                    $("#DisplayMsg").append(msg.trim());
                    $("#ArchivoCargado").fadeIn(600);
                break;
            }
        });
    }
    $("#thisFile").click(function () {
        $("#FileName").trigger('click');
    });
</script>

<div class="panel panel-primary">
    <div class="panel-body" style='width:100%; text-align:center; margin-top:100px;'>
        <form name="frmCarga" id="frmCarga" method="post" enctype="multipart/form-data">
            <input type='file' id='FileName' name='FileName' style='display:none' title='CargaXML' onchange='getZipFile(this)'/>
            <?php
                switch($thisType){
                    case "CERT_TIM":
                        $ButtonColor = "primary";
                        $ButtonTitle = "Cargar Archivo Timbrado .zip";
                    break;
                    case "CERT_SEP":
                        $ButtonColor = "warning";
                        $ButtonTitle = "Cargar Archivo SEP .zip";
                    break;
                    case "TIT_TIM":
                        $ButtonColor = "purple";
                        $ButtonTitle = "Cargar Archivo Timbrado .zip";
                    break;
                    case "TIT_SEP":
                        $ButtonColor = "orange";
                        $ButtonTitle = "Cargar Archivo SEP .zip";
                    break;
                } 
                echo "<button type='button' id='thisFile' class='btn btn-".$ButtonColor." text-white' style='padding:15px;' onchange='getZipFile(this)'>";
                echo "    <i class='fa fa-file-archive-o'></i>";
                echo "    <span>".$ButtonTitle."</span>";
                echo "</button>";
            ?>
            <div align='left' class='imagediv'> 
                <input id='selectfile' type='file' name='image' style='display: none;' onchange='getZipFile(this)' />
            </div>
        </form>	
    </div>
    <div id='MensajeResultado' class="panel panel-primary">
        <div class='tab-content' id='ArchivoCargado' style='display:none;'>
            <div id='CheckData' style='background-color:#F1f2f3; display:flex; width:50%; margin:auto; padding:20px; justify-content:center; align-items:center;'><h4>Resultado de Archivos cargados:</h4></div>
            <div id='CheckData' style='background-color:#F1f2f3; display:flex; width:50%; min-height:50px; margin:auto; padding:0 50px 20px; justify-content:center; align-items:center;'><span id='DisplayMsg'></span></div>
        </div> 
    </div>
</div>