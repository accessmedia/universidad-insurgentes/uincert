<?php
session_start();
$SecureSection = true;
$_SESSION["SectionType"] = "Cert-Tit";
require_once("util/utilerias.php");
if(isset($_SESSION['susu'])){
    ?>    
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
        <title>Universidad Insurgentes</title>
        <link rel="icon" href="./img/uin.ico" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/site.css">
        <link rel="stylesheet" href="css/menu.css" type="text/css">
        <script src="./js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="./js/dist/jquery.fancybox.css" type="text/css">
        <script type='text/javascript' src='./js/dist/jquery.fancybox.js'></script>
        <script type='text/javascript' src='./js/tablax2.js'></script>

        <script>
            function cargaContenido(pag, thisType){
                $("#container").html('');
                $("#container").load(pag+'.php?SecType='+thisType);
            }

            function cargaContenido2(pag){ 
                $("#datatable").html('');  
                $("#datatable").load(pag +'.php?vCampus=' + document.getElementById("cmbCampus").value + '&vAnho=2018&vFInicio='+ document.getElementById("finicio").value +'&vFFinal='+ document.getElementById("ffin").value + '' );
                
                find.$("#cmbCampus").val('');
                find.$("#finicio").val('');
                find.$("#ffin").val('');
            }

            function cargaContenido3(pag){
                $("").html('');
                $("").load(pag +'.php');
            } 

        function sleep (time) {
          return new Promise((resolve) => setTimeout(resolve, time));
      }   
  </script>          
</head>
<body>
    <?php
        $obj = new Utilerias;
        $obj -> menu($_SESSION["SectionType"]);
    ?>

    <div class="clearfix"></div>
    <div class="menu"></div>
    <div class="clearfix"></div>

    <div id="container" class="" style="width:100%; /*margin-top:20px;*/ padding: 2% 5%; justify-content:center;">
        <div id="x"  style="width: 100%; height: 82%; display: flex; justify-content: center; align-items: center;">
            <div class="tab-content" id="pills-tabContent">
                <div class="home section tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="row">
                        <div class="col-md-5 left">
                            <h2 class="text-primary">INICIO</h2>
                            <span>SOY YO,</span> <span class="soy-uin">SOY UIN</span>
                        </div>
                        <div class="col-md-5 right">
                            <img src="img/central.png">
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <hr>
        <footer>
            <p>© 2019 - Portal</p>
        </footer>              
    </div>

    <div id="datatable">
    </div>

    <!-- Modal -->
    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 1024px !important;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 idclass="modal-title" id="infoModalTitle"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="infoModalBody" class="modal-body">
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php
    //echo  getRealIP();
}else{
    header("Location: index.php");
}




function getRealIP()
{

    if (isset($_SERVER["HTTP_CLIENT_IP"]))
    {
        return $_SERVER["HTTP_CLIENT_IP"];
    }
    elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
    {
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    }
    elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
    {
        return $_SERVER["HTTP_X_FORWARDED"];
    }
    elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
    {
        return $_SERVER["HTTP_FORWARDED_FOR"];
    }
    elseif (isset($_SERVER["HTTP_FORWARDED"]))
    {
        return $_SERVER["HTTP_FORWARDED"];
    }
    else
    {
        return $_SERVER["REMOTE_ADDR"];
    }

}



?>