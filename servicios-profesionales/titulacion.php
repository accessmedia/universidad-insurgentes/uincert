<?php
    $GLOBALS['thisSPId'] = 3;
    function MenuInterno(){
        echo "
            <a href='?ty=".$_REQUEST["ty"]."&SecId=solicitudes-validacion' class='list-group-item'><i class='fa fa-warning'></i> Actualizaci&oacute;n de Datos</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=servicios-vigentes' class='list-group-item'><i class='fa fa-file'></i> Autorizaci&oacute;n de Documentos</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=titulos-electronicos' class='list-group-item'><i class='fa fa-file-pdf-o'></i> Registro de Tr&aacute;mites</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=titulos-fisicos' class='list-group-item'><i class='fa fa-file-text'></i> T&iacute;tulos F&iacute;sicos</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=servicios-concluidos' class='list-group-item'><i class='fa fa-check'></i> T&iacute;tulos Concluidos</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=servicios-cancelados' class='list-group-item'><i class='fa fa-times'></i> Procesos Cancelados</a>
        ";
    }
    
    function MuestraContenido(){
        $obj = new ServProf;
        switch ($_REQUEST["SecId"]) {
            case 'solicitudes-validacion':
            case 'registros-tramite':
            case 'servicios-vigentes':
            case 'titulos-electronicos':
            case 'titulos-fisicos':
            case 'servicios-concluidos':
            case 'servicios-cancelados':
                Validaciones($_REQUEST["SecId"]);
                break;
            case 'info-alumno':
                InfoAlumno($_POST["thisId"]);
                break;
            case 'actualiza-data':
            case 'nuevo-convenio':
                $obj->UpdateInfo($_POST["SecId"], $_POST["thisId"]);
                break;
            case 'historico':
                InfoAlumno($_REQUEST["regId"]);
                break;
            case 'generar-titulo':
            case 'notificar-titulo':
                $obj->GeneraDocumentos($_REQUEST["SecId"], $_REQUEST["StudentId"]);
                break;
            case 'validar-documentos':
                ValidaDocumentos($_REQUEST["StudentId"]);
                break;
            default:
                break;
        }
    }
    
    function ContenidoPrincipal(){
        Validaciones("solicitudes-validacion");
    }
    
    function Validaciones($thisType){
        $obj = new ServProf;
        switch ($thisType) {
            case 'solicitudes-validacion':
                $DisplayButtons     = 1;
                $thisTitle          = "Actualizaci&oacute;n de Datos";

                $thisTable_1        = "ACC_CAMBIO_DATOS_SPR_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(1,4,7,10,11) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'servicios-vigentes':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de registros por autorizar documentos";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(7,10,19,26,28) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'titulos-electronicos':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Registro de alumnos en tr&aacute;mite de t&iacute;tulo";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(49) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'titulos-fisicos':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de alumnos en espera de entrega de su T&iacute;tulo F&iacute;sico";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(50) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'servicios-concluidos':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de alumnos con procesos de Titulaci&oacute;n concluidos";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(51) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
            case 'servicios-cancelados':
                $DisplayButtons     = 1;
                $DisplayData        = 0;
                $thisTitle          = "Listado de alumnos con procesos de Titulaci&oacute;n canceladas";

                $thisTable_1        = "ACC_SERVICIOS_PROFESIONALES_SP";
                $ThisSqlFilter_1    = "WHERE ESTATUS_ID IN(21) AND SERVICIO_PROFESIONAL_ID = ".$GLOBALS["thisSPId"];
                break;
        }
        echo "
            <div class='info' style='padding:10px; margin-left:0px; margin-top:0px; text-align:left;'>
                <h3 class='page-header'>".$thisTitle."</h3>
                <div id='content' class='content'>
                    <p>Seleccione desde el listado de alumnos que han realizado una solicitud de revisi&oacute;n de informaci&oacute;n el registro de su inter&eacute;s:
                </div>
            </div>
            <table id='TablaSolicitudesPendientes' class='table table-rounded table-striped table-sm' cellspacing='0' width='100%' style='text-align:center;'>
                <thead>";
                switch ($thisType) {
                    case 'solicitudes-validacion':
                    case 'registros-tramite':
                        echo " 
                            <tr class='bg-primary rounded-top text-white' style='text-align:center;'>
                                <th class='col-xs-2'>#</th>
                                <th class='col-xs-2'>Tipo</th>
                                <th class='col-xs-2'>Id Alumno</th>
                                <th class='col-xs-2'>Nombre</th>
                                <th class='col-xs-2'>A.Paterno</th>
                                <th class='col-xs-2'>A.Materno</th>
                                <th class='col-xs-2'>Titulaci&oacute;n</th>
                                <th class='col-xs-2'>Estatus</th>
                                <th class='col-xs-2'>Pagado</th>
                                <th class='col-xs-2'>Fecha</th>
                                <th class='col-xs-2'>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>";
                            echo $obj->SolicitudesValidacion($thisTable_1, $ThisSqlFilter_1, $DisplayButtons);
                        echo "</tbody>";
                        break;
                    case 'servicios-vigentes':
                    case 'titulos-electronicos':
                    case 'titulos-fisicos':
                    case 'servicios-concluidos':
                    case 'servicios-cancelados':
                        echo "
                            <tr class='bg-primary rounded-top text-white' style='text-align:center;'>
                                <th class='col-xs-2'>Alumno</th>
                                <th class='col-xs-2'>Carrera</th>
                                <th class='col-xs-2'>Convenio</th>
                                <th class='col-xs-2'>Estatus</th>
                                <th class='col-xs-2'>Pagado</th>
                                <th class='col-xs-2'>Titulaci&oacute;n</th>";
                                if($thisType === "titulos-electronicos"){
                                    echo "<th class='col-xs-2'>Estatus</th>";
                                }
                                echo "<th class='col-xs-2'>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            ".$obj->RegistrosServiciosProfesionales($thisTable_1, $ThisSqlFilter_1, $DisplayButtons, $DisplayData)."
                        </tbody>
                        ";
                        break;
                }
            echo "</table>";
    }
?>