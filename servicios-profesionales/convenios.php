<?php
    $GLOBALS['thisSPId'] = 4;
    function MenuInterno(){
        echo "
            <a href='?ty=".$_REQUEST["ty"]."&SecId=alta-convenio' class='list-group-item'><i class='fa fa-gears'></i> Alta de Nuevo Convenio</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=busca-convenio' class='list-group-item'><i class='fa fa-check'></i> Convenios Autorizados</a>
            <a href='?ty=".$_REQUEST["ty"]."&SecId=convenios-proceso' class='list-group-item'><i class='fa fa-warning'></i> Convenios Desactivados</a>
        ";
    }
    
    function MuestraContenido(){
        $obj = new ServProf;
        switch ($_REQUEST["SecId"]) {
            case 'alta-convenio':
                HeaderConvenios($_REQUEST["SecId"]);
                $obj->FormularioConvenio("WHERE ID = 0");
                break;
            case 'editar-convenio':
                HeaderConvenios($_REQUEST["SecId"]);
                $obj->FormularioConvenio("WHERE ID = ".$_REQUEST["thisid"]);
                break;
            case 'busca-convenio':
            case 'convenios-autorizados':
            case 'convenios-proceso':
                MuestraConvenios($_REQUEST["SecId"]);
                break;
        }
    }
    
    function ContenidoPrincipal(){
        MuestraConvenios('busca-convenio');
    }

    function HeaderConvenios($thisType){
        $obj = new ServProf;
        $RcdDate = $obj->formatDate(date("Y-m-d"));
        switch ($thisType) {
            case 'alta-convenio':
                $thisTitle  = "ALTA DE NUEVO CONVENIO";
                $TxtMsg     = "<h6>Ingrese los datos necesarios para dar de alta un nuevo Convenio</h6>";
                break;
            case 'editar-convenio':
                $thisTitle  = "EDITAR CONVENIO";
                $TxtMsg     = "<h6>Ingrese los datos necesarios corregir la informaci&oacute;n de este Convenio</h6>";
                break;
            case 'convenios-autorizados':
                $thisTitle  = "CONVENIOS AUTORIZADOS";
                $TxtMsg     = "<h6>Listado de Convenios Autorizados</h6>";
                break;
            case 'convenios-proceso':
                $thisTitle  = "CONVENIOS EN PROCESO";
                $TxtMsg     = "<h6>Listado de Convenios en proceso de autorizaci&oacute;n</h6>";
                break;
            case 'busca-convenio':
                $thisTitle  = "B&Uacute;SQUEDA DE CONVENIOS";
                $TxtMsg     = "<h6>Ingrese en el campo de b&uacute;squeda el nombre de la empresa o nombre del responsable total o parcial.</h6>";
                break;
        }
        echo "
            <div class='info' style='padding:0px; text-align:left;'>
                <div id='invoice'>
                    <div class='invoice overflow-auto'>
                        <div style='min-width: 600px'>
                            <div class='row contacts'>
                                <div class='col invoice-to'>
                                    <div class='text-gray-light'>ADMINISTRADOR DE CONVENIOS:</div>
                                    <h2 class='to'>".$thisTitle."</h2>
                                    <div class='address'>Universidad Insurgentes | Unidad de Servicios Escolares</div>
                                    <div class='email'><a href='mailto:correo@uinsurgentes.com.mx'>correo@uinsurgentes.com.mx</a></div>
                                </div>
                                <div class='col invoice-details'>
                                    <h6 class='to'>XXX-XX-X</h6>
                                    <h1 class='invoice-id'></h1>
                                    <div class='date'>Fecha Actual: ".$RcdDate."</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id='content' class='content'>
                    ".$TxtMsg."
                </div>
            </div>
        ";
    }
    
    function MuestraConvenios($thisType){
        $obj = new ServProf;
        HeaderConvenios($thisType);
        switch ($thisType) {
            case 'alta-convenio':
                $thisFilter = "";
                break;
            case 'convenios-autorizados':
                $thisFilter = "WHERE NOMBRE_EMPRESA LIKE '%".$_REQUEST["busqueda"]."%' OR REPONSABLE LIKE '%".$_REQUEST["busqueda"]."%' AND ESTATUS_ID = 1";
                break;
            case 'convenios-proceso':
                $thisFilter = "WHERE ESTATUS_ID = 0";
                break;
        }
        echo "
            <div class='info' style='padding:0px; text-align:left;'>
                <br />
                <div class='row' style='width:100%; text-align:center; margin-top:0px; margin-bottom:20px;'>
                    <div class='row' style='width:600px; margin:auto;'>
                        <form id='BuscaConvenios' action='index.php' method='post' class='form-control' style='border:0px;'>
                            <div class='input-group md-form form-sm form-1 pl-0'>
                                <input type='hidden' id='ty' name='ty' value='".$_REQUEST["ty"]."'>
                                <input type='hidden' id='SecId' name='SecId' value='convenios-autorizados'>
                                <input type='text' id='busqueda' name='busqueda' class='form-control my-0 py-1' placeholder='B&uacute;squeda por Nombre de Empresa o Persona Responsable' aria-label='B&uacute;squeda' style='border-radius: 1em; height: 50px;' required>
                                <div class='input-group-prepend'>
                                    <button type='submit' class='btn btn-primary btn-sm' style='border-radius: 1em; height: 50px;'><i class='fa fa-search fa-fw fa-lg m-r-3'></i> BUSCAR</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                ";
                switch ($thisType) {
                    case 'busca-convenio':
                        break;
                    default;
                        if(isset($_REQUEST["busqueda"])){
                            $SearchResult = "<h4>Resultados obtenidos a partir del criterio de b&uacute;squeda: <b>".$_REQUEST["busqueda"]."</b></h4><br>";
                        }else{
                            $SearchResult = "";
                        }
                        echo "
                            ".$SearchResult."
                            <table id='TablaConvenios' class='table table-rounded table-striped table-sm' cellspacing='0'>
                                <thead>
                                    <tr class='bg-primary rounded-top text-white'>
                                        <th class='col-xs-2'>#</th>
                                        <th class='col-xs-2'>Nombre de la Empresa</th>
                                        <th class='col-xs-2'>Direcci&oacute;n</th>
                                        <th class='col-xs-2'>Tel&eacute;fono</th>
                                        <th class='col-xs-2'>Sector</th>
                                        <th class='col-xs-2'>Código Postal</th>
                                        <th class='col-xs-2' style='text-align:center;'>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    ".$obj->ListadoConvenios($thisType, $thisFilter)."
                                </tbody>
                            </table>
                        ";
                        $obj->ListadoConvenios($thisType, $thisFilter);
                        break;
                }
            echo "</div>
        ";
    }
?>