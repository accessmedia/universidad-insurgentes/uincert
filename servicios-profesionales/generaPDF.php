<?php
    $thisPath       = __DIR__;
    $SecureSection  = true;

    require_once($thisPath."/vendor/autoload.php");
    require_once("util/mod-servprof.php");

    $thisId             = $_REQUEST["StudentId"];
    $thisType           = $_REQUEST["ThisSection"];
    $thisPeopleCodeId   = $_REQUEST["PeopleCodeId"];
    $thisFileName       = $_REQUEST["RegName"];

    $thisPath       = $thisPath."/docs/".$thisPeopleCodeId;
    $geCssStyle     = file_get_contents('css/style.css');
    
    if(!file_exists($thisPath)){mkdir($thisPath, 0777, true);}

    $obj    = new ServProf;
    $mpdf   = new \Mpdf\Mpdf();
    $mpdf->falseBoldWeight = 9;

    $ThisPdfContent = $obj->FormatosCartas($thisType, $thisId, "");

    $mpdf->writeHtml($geCssStyle, \Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->writeHtml($ThisPdfContent);
    $mpdf->output($thisPath.'/'.$thisFileName, 'F');
?>