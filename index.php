<?php 
session_start();
if (!isset($_SESSION['susu'])) {
    ?>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Universidad Insurgentes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="icon" href="./img/uin.ico"/>
        <!-- <link rel="stylesheet" href="css/login.css" type="text/css"> -->
        <link rel='stylesheet' id='compiled.css-css' href='./css/compiled-4.5.15.min.css' type='text/css' media='all'/>
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="./css/site.css">
        <script type='text/javascript' src='./js/compiled.0.min.js?ver=4.5.15'></script>
        <!--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <script src="js/jquery.min.js"></script>
        <script>
            function Validar() {
                var usu = $('input:text[name=usu]').val();
                var pass = $('input:password[name=pass]').val();
                $('#login').hide();
                $.ajax({
                    url: "check.php",
                    type: "POST",
                    data: {usu: usu, pass: pass},
                    success: function (resp) {
                        //alert(resp);
                        if (resp != 0) {
                            window.location = 'index2.php';
                        } else {
                            //$('#resultado').html(resp);
                            $('#login').show();
                            $('input[type="text"]').val('');
                            $('input[type="password"]').val('');
                            $("#error").text("Usuario o contraseña incorrectos");
                        }
                    }
                });
            }
        </script>
    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top bg-primary" style="min-height: 70px">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/" style="">
                    <img src="img/logo_uin.png" style="width: 200px">
                </a>
            </div>
            <nav class="navbar navbar-expand-lg navbar-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </nav>
        </div>
    </nav>

    <div class="container body-content">
        <h2>Entrar</h2>

        <div class="row login">
            <div class="col-md-4">
                <section>
                    <form method="POST" action="return false" onSubmit="return false">
                        <hr>
                        <div class="text-danger validation-summary-valid" data-valmsg-summary="true">
                            <ul>
                                <li style="display:none"></li>
                            </ul>
                        </div>
                        <div id="error" align="left" class="mb-2" style="color: red;">
                        </div>
                        <div class="form-group">
                            <input type="text" id="usu" class="form-control valid fadeIn second" name="usu"
                                   placeholder="Usuario" value="">
                            <span class="text-danger field-validation-valid" data-valmsg-for="Email"
                                  data-valmsg-replace="true"></span>
                        </div>
                        <div class="form-group">
                            <input type="password" id="pass" class="form-control valid fadeIn third" name="pass"
                                   placeholder="Contraseña" value="">
                            <span class="text-danger field-validation-valid" data-valmsg-for="Password"
                                  data-valmsg-replace="true"></span>
                        </div>
                        <div class="form-group">
                            <button type='submit' class='btn btn-default' onClick='Validar();' value='Iniciar Sesión'
                                    value="ff0d682466426e09a0127410604aeeba1c4b404d">Entrar
                            </button>
                        </div>

                    </form>
                </section>
            </div>

            <div class="col-md-6 col-md-offset-2 text-center">
                <section>
                    <img src="img/central.png">
                </section>
            </div>
        </div>


        <hr>
        <footer>
            <p>© 2019 - Portal</p>
        </footer>
    </div>

    <!-- html comment

            <div id="login" class="wrapper fadeInDown">
                <div id="formContent">
                    <div class="fadeIn first" style="text-align:center;">
                        <img src="assets/img/logov.png" width="90" height="88" longdesc="http://qwewwqe" />
                    </div>
                    <form method="POST" action="return false" onSubmit="return false">
                        <input type="text" id="usu" class="fadeIn second" name="usu" placeholder="Usuario">
                        <input type="password" id="pass" class="fadeIn third" name="pass" placeholder="Contraseña">
                        <br>
                        <br>
                        <input type='submit' class='btn btn-primary' onClick='Validar();' value='Iniciar Sesión' style='padding:5px; padding:15px 80px;' value="ff0d682466426e09a0127410604aeeba1c4b404d">
                    </form>
                    <div id="error" align="center" style="color: red;">
                    </div>
                </div>
            </div>

    -->

    <div id="resultado" align="center">

    </div>
    </body>
    </html>
    <?php
} else {
    header("Location: index2.php");
}
?>




