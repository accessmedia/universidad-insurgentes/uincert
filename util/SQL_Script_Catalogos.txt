

 --Entidad federativa
   NO HAY TABLA

 --Observaciones
 select * from OBSERVACION_CERTIFICADO

 --Catalogo version 2.0

 --Cargo firmante 
 select * from NODO_RESPONSABLE

 --Tipo de periodo
 --select * from PERIODO_CERTIFICADO
 --Catalogo version 2.0

 --Genero NO HAY TABLA

 --Tipo de certificacion

 --Tipo de certificacion campus 
 select * from ENTIDAD_CERTIFICADO

 --Asignaturas
 select * from PROGRAMA_CERTIFICADO

 --Carreras 
 select * from CARRERA_CERTIFICADO

 --RVOE
 select * from PROGRAMA_CERTIFICADO

 --NO HAY TABLAS
 --Estudio Antecedente
 --Fundamento Legar Servicio Social
 --Modalidad Titulacion
 --Autorizacion Reconocimiento


--SCRIPT PARA CREACION DE CATALOGOS


--Entidad federativa

 	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO
	CREATE TABLE [dbo].[Cat_Entidad_Federativa](
		[id] [int] IDENTITY(1,1) NOT NULL,
		[ID_ENTIDAD_FEDERATIVA] [varchar](10) NULL,
                [C_NOM_ENT] [varchar](50) NULL,
                [C_ENTIDAD_ABR] [varchar](50) NULL
	) ON [PRIMARY]
	GO

-- Insertar Estados a Cat_Entidad_Federativa

  INSERT INTO Cat_Entidad_Federativa
  (ID_ENTIDAD_FEDERATIVA,C_NOM_ENT,C_ENTIDAD_ABR)
VALUES
  ('01','AGUASCALIENTES','AGS'),
  ('02','BAJA CALIFORNIA','BC'), 
  ('03','BAJA CALIFORNIA SUR','BCS'), 
  ('04','CAMPECHE','CAMP'), 
  ('05','COAHUILA DE ZARAGOZA','COAH'), 
  ('06','COLIMA','COL'), 
  ('07','CHIAPAS','CHIS'), 
  ('08','CHIHUAHUA','CHIH'), 
  ('09','CIUDAD DE MÉXICO','CDMX'), 
  ('10','DURANGO','DGO'), 
  ('11','GUANAJUATO','GTO'), 
  ('12','GUERRERO','GRO'), 
  ('13','HIDALGO','HGO'), 
  ('14','JALISCO','JAL'), 
  ('15','MÉXICO','MEX'), 
  ('16','MICHOACÁN DE OCAMPO','MICH'), 
  ('17','MORELOS','MOR'), 
  ('18','NAYARIT','NAY'), 
  ('19','NUEVO LEÓN','NL'),  
  ('20','OAXACA','OAX'),
  ('21','PUEBLA','PUE'), 
  ('22','QUERÉTARO','QRO'), 
  ('23','QUINTANA ROO','QROO'), 
  ('24','SAN LUIS POTOSÍ','SLP'), 
  ('25','SINALOA','SIN'), 
  ('26','SONORA','SON'), 
  ('27','TABASCO','TAB'), 
  ('28','TAMAULIPAS','TAMPS'), 
  ('29','TLAXCALA','TLAX'),  
  ('30','VERACRUZ DE IGNACIO DE LA LLAVE','VER'),
  ('31','YUCATÁN','YUC'),  
  ('32','ZACATECAS','ZAC');

  -- Cargo Firmante Autorizados

      SET ANSI_NULLS ON
  GO
  SET QUOTED_IDENTIFIER ON
  GO
  CREATE TABLE [dbo].[Cat_Cargo_Firmante_Autorizado](
    [id] [int] IDENTITY(1,1) NOT NULL,
    [ID_CARGO] [varchar](10) NULL,
                [CARGO_FIRMANTE] [varchar](100) NULL
  ) ON [PRIMARY]
  GO

        INSERT INTO [Cat_Cargo_Firmante_Autorizado]
  (ID_CARGO,CARGO_FIRMANTE)
VALUES
  ('1','DIRECTOR'),
  ('2','SUBDIRECTOR'), 
  ('3','RECTOR'), 
  ('4','VICERRECTOR'), 
  ('5','RESPONSABLE DE EXPEDICIÓN'), 
  ('6','SECRETARIO GENERAL'), 
  ('7','AUTORIDAD LOCAL'), 
  ('8','AUTORIDAD FEDERAL');

     -- Genero

      SET ANSI_NULLS ON
  GO
  SET QUOTED_IDENTIFIER ON
  GO
  CREATE TABLE [dbo].[Cat_Genero](
    [id] [int] IDENTITY(1,1) NOT NULL,
    [idGenero] [varchar](10) NULL,
                [Genero] [varchar](50) NULL
  ) ON [PRIMARY]
  GO

        INSERT INTO [Cat_Genero]
  (idGenero,genero)
VALUES
  ('250','HOMBRE'),
  ('251','MUJER');
 
 --Estudio Antecedente

    SET ANSI_NULLS ON
  GO
  SET QUOTED_IDENTIFIER ON
  GO
  CREATE TABLE [dbo].[Cat_Estudio_Antecedente](
    [id] [int] IDENTITY(1,1) NOT NULL,
    [ID_TIPO_ESTUDIO_ANTECEDENTE] [varchar](10) NULL,
                [TIPO_ESTUDIO_ANTECEDENTE] [varchar](50) NULL,
                [TIPO_EDUCATIVO_DEL_ANTECEDENTE] [varchar](50) NULL
  ) ON [PRIMARY]
  GO

      INSERT INTO Cat_Estudio_Antecedente
  (ID_TIPO_ESTUDIO_ANTECEDENTE,TIPO_ESTUDIO_ANTECEDENTE,TIPO_EDUCATIVO_DEL_ANTECEDENTE)
VALUES
  ('1','MAESTRÍA','EDUCACIÓN SUPERIOR'),
  ('2','LICENCIATURA','EDUCACIÓN SUPERIOR'), 
  ('3','TÉCNICO SUPERIOR UNIVERSITARIO','EDUCACIÓN SUPERIOR'), 
  ('4','BACHILLERATO','EDUCACIÓN MEDIA SUPERIOR'), 
  ('5','EQUIVALENTE A BACHILLERATO','EDUCACIÓN MEDIA SUPERIOR'), 
  ('6','SECUNDARIA','EDUCACIÓN BÁSICA');

  --Nivel de estudios

  DROP TABLE Cat_Nivel_de_estudios;
      SET ANSI_NULLS ON
  GO
  SET QUOTED_IDENTIFIER ON
  GO
  CREATE TABLE [dbo].[Cat_Nivel_de_estudios](
    [id] [int] IDENTITY(1,1) NOT NULL,
                [idNivelEstudios] [varchar](50) NULL,
                [descripcion] [varchar](50) NULL
  ) ON [PRIMARY]
  GO

      INSERT INTO Cat_Nivel_de_estudios
  (idNivelEstudios,descripcion)
VALUES
  ('95','DOCTORADO'),
  ('85','ESPECIALIDAD'), 
  ('84','TÉCNICO SUPERIOR UNIVERSITARIO'), 
  ('83','PROFESIONAL ASOCIADO'), 
  ('82','MAESTRÍA'), 
  ('81','LICENCIATURA');


 --Modalidad Titulacion

    SET ANSI_NULLS ON
  GO
  SET QUOTED_IDENTIFIER ON
  GO
  CREATE TABLE [dbo].[Cat_Modalidad_Titulacion](
    [id] [int] IDENTITY(1,1) NOT NULL,
    [ID_MODALIDAD_TITULACION] [varchar](10) NULL,
                [MODALIDAD_TITULACION] [varchar](50) NULL,
                [TIPO_DE_MODALIDAD] [varchar](50) NULL
  ) ON [PRIMARY]
  GO

    INSERT INTO Cat_Modalidad_Titulacion
  (ID_MODALIDAD_TITULACION,MODALIDAD_TITULACION,TIPO_DE_MODALIDAD)
VALUES
  ('1','POR TESIS','CONSTANCIA DE EXENCIÓN'),
  ('2','POR PROMEDIO','CONSTANCIA DE EXENCIÓN'), 
  ('3','POR ESTUDIOS DE POSGRADOS','CONSTANCIA DE EXENCIÓN'), 
  ('4','POR EXPERIENCIA LABORAL','CONSTANCIA DE EXENCIÓN'), 
  ('5','POR CENEVAL','CONSTANCIA DE EXENCIÓN'), 
  ('6','OTRO','CONSTANCIA DE EXENCIÓN');


  
  
  
  
  
 
  
  
   
  
  
  
 
  
  
  
  

 
  
  
  


































